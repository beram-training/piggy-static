<?php

declare(strict_types=1);

use beram\PiggyStatic\DependencyInjection\Container;
use beram\PiggyStatic\DependencyInjection\Definition as Def;
use beram\PiggyStatic\MarkupLanguage\Markdawn\Compiler;
use beram\PiggyStatic\MarkupLanguage\Markdawn\Interpreter;
use beram\PiggyStatic\MarkupLanguage\Markdawn\Lexer;
use beram\PiggyStatic\MarkupLanguage\Markdawn\Parser;
use beram\PiggyStatic\WebsiteGenerator\Command\Generate;
use beram\PiggyStatic\WebsiteGenerator\Layout\Renderer;
use beram\PiggyStatic\WebsiteGenerator\Source\SourceLocator;

return new Container([
    Lexer::class => new Def(fn() => new Lexer()),
    Parser::class => new Def(fn() => new Parser()),
    Compiler::class => new Def(fn() => new Compiler()),
    Interpreter::class => new Def(fn(Container $container) => new Interpreter(
        $container->get(Lexer::class),
        $container->get(Parser::class),
        $container->get(Compiler::class)
    )),
    SourceLocator::class => new Def(fn(Container $container) => new SourceLocator($container->get(Interpreter::class))),
    Renderer::class => new Def(fn(Container $container) => new Renderer()),
    Generate::class => new Def(fn(Container $container) => new Generate($container->get(SourceLocator::class), $container->get(Renderer::class))),
]);
