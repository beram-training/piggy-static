<?php

declare(strict_types=1);

namespace Tests\beram\PiggyStatic\DependencyInjection;

use beram\PiggyStatic\DependencyInjection\Container;
use beram\PiggyStatic\DependencyInjection\Definition;
use beram\PiggyStatic\DependencyInjection\Exception\CircularDependencyException;
use beram\PiggyStatic\DependencyInjection\Exception\ServiceNotFoundException;
use PHPUnit\Framework\TestCase;

final class ContainerTest extends TestCase
{
    public function test_get_service_not_found(): void
    {
        $container = new Container([]);

        self::expectException(ServiceNotFoundException::class);
        self::expectExceptionMessage('Service "unknown" does not exist.');

        $container->get('unknown');
    }

    public function test_get_circular_dependency(): void
    {
        $container = new Container([
            FooCircularDependency::class => new Definition(static fn (Container $container): FooCircularDependency => new FooCircularDependency($container->get(BarCircularDependency::class))),
            BarCircularDependency::class => new Definition(static fn (Container $container): BarCircularDependency => new BarCircularDependency($container->get(FooCircularDependency::class))),
        ]);

        self::expectException(CircularDependencyException::class);
        self::expectExceptionMessage(\sprintf('Circular dependency detected: %s > %s > %s.', FooCircularDependency::class, BarCircularDependency::class, FooCircularDependency::class));

        $container->get(FooCircularDependency::class);
    }

    public function test_get(): void
    {
        $container = $this->defaultContainer();

        self::assertInstanceOf(Bar::class, $container->get(Bar::class));
    }

    public function test_has(): void
    {
        $container = $this->defaultContainer();

        self::assertTrue($container->has(Foo::class));
        self::assertTrue($container->has(Bar::class));
        self::assertFalse($container->has('unknown'));
    }

    private function defaultContainer(): Container
    {
        return new Container([
            Foo::class => new Definition(static fn (): Foo => new Foo()),
            Bar::class => new Definition(static fn (Container $container): Bar => new Bar($container->get(Foo::class))),
        ]);
    }
}

final class FooCircularDependency
{
    public function __construct(
        private readonly BarCircularDependency $bar
    ) {
    }
}

final class BarCircularDependency
{
    public function __construct(
        private readonly FooCircularDependency $foo
    ) {
    }
}

final class Foo
{
}
final class Bar
{
    public function __construct(
        private readonly Foo $foo
    ) {
    }
}
