<?php

declare(strict_types=1);

namespace Tests\beram\PiggyStatic\CommandLine;

use beram\PiggyStatic\CommandLine\Command;
use beram\PiggyStatic\CommandLine\Config;
use beram\PiggyStatic\CommandLine\ExitCode;
use beram\PiggyStatic\CommandLine\Input;
use beram\PiggyStatic\CommandLine\Input\AllowedOption;
use beram\PiggyStatic\CommandLine\Output;
use beram\PiggyStatic\CommandLine\Test\CommandTester;
use beram\PiggyStatic\CommandLine\Test\TestOutput;
use PHPUnit\Framework\TestCase;

final class CommandTest extends TestCase
{
    /**
     * @dataProvider provider
     */
    public function test(string $expected, array $options): void
    {
        $command = new class() extends Command {
            protected function configure(): Config
            {
                return Config::default('hello')
                    ->withOption('name', AllowedOption\Value::required)
                    ;
            }

            protected function execute(
                Input $input,
                Output $output,
            ): ExitCode {
                $name = $input->options->has('name') ? $input->options->get('name') : 'world';
                $output->out(\sprintf('Hello %s', $name));

                return ExitCode::OK;
            }
        };

        $commandTester = new CommandTester();
        $output = new TestOutput();
        $exitCode = $commandTester->execute($command, new Input(new Input\Options($options)), $output);
        self::assertSame(ExitCode::OK, $exitCode);
        self::assertSame(
            [$expected],
            $output->getOut(),
        );
    }

    public function provider(): \Generator
    {
        yield [
            'Hello world',
            [],
        ];
        yield [
            'Hello bob',
            ['name' => 'bob'],
        ];
    }
}
