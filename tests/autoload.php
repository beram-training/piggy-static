<?php

declare(strict_types=1);

include_once __DIR__.'/../autoload.php';

\spl_autoload_register(function (string $class): void {
    $classFile = \str_replace(
        ['Tests\\beram\\PiggyStatic\\', '\\'],
        ['', '/'],
        $class
    );
    $path = __DIR__.'/tests/'.$classFile.'.php';
    if (\file_exists($path)) {
        include $path;
    }
});
