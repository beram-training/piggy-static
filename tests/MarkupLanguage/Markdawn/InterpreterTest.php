<?php

declare(strict_types=1);

namespace Tests\beram\PiggyStatic\MarkupLanguage\Markdawn;

use beram\PiggyStatic\MarkupLanguage\Markdawn\Compiled\Document;
use beram\PiggyStatic\MarkupLanguage\Markdawn\Compiled\Html;
use beram\PiggyStatic\MarkupLanguage\Markdawn\Compiled\Metadata;
use beram\PiggyStatic\MarkupLanguage\Markdawn\Compiler;
use beram\PiggyStatic\MarkupLanguage\Markdawn\Interpreter;
use beram\PiggyStatic\MarkupLanguage\Markdawn\Lexer;
use beram\PiggyStatic\MarkupLanguage\Markdawn\Parser;
use PHPUnit\Framework\TestCase;

final class InterpreterTest extends TestCase
{
    /**
     * @dataProvider fileToHtmlStringProvider
     */
    public function test_file_to_html_string(string $expected, string $filepath): void
    {
        self::assertSame(\file_get_contents($expected), (string) (new Interpreter(new Lexer(), new Parser(), new Compiler()))->fileToDocument($filepath)->html);
    }

    public function fileToHtmlStringProvider(): \Generator
    {
        yield $this->caseFileToHtmlString('simple_strong');
        yield $this->caseFileToHtmlString('multiple_strong');
        yield $this->caseFileToHtmlString('strong_emphasis_strikethrough');
        yield $this->caseFileToHtmlString('images');
        yield $this->caseFileToHtmlString('links');
        yield $this->caseFileToHtmlString('code');
        yield $this->caseFileToHtmlString('all');
    }

    private function caseFileToHtmlString(string $id): array
    {
        return [
            \sprintf(__DIR__.'/fixtures/interpreter/%s.html', $id),
            \sprintf(__DIR__.'/fixtures/interpreter/%s.md', $id),
        ];
    }

    public function test_file_to_document(): void
    {
        [$html, $md] = $this->caseFileToHtmlString('all');
        self::assertEquals(
            new Document(
                new Html(\file_get_contents($html)),
                new Metadata([
                   'layout' => new Metadata\Property('layout', 'foo'),
                   'title' => new Metadata\Property('title', 'bar'),
                ]),
            ),
            (new Interpreter(new Lexer(), new Parser(), new Compiler()))->fileToDocument($md)
        );
    }
}
