<?php

declare(strict_types=1);

namespace Tests\beram\PiggyStatic\MarkupLanguage\Markdawn;

use beram\PiggyStatic\MarkupLanguage\Markdawn\Compiled;
use beram\PiggyStatic\MarkupLanguage\Markdawn\Compiler;
use beram\PiggyStatic\MarkupLanguage\Markdawn\Node\Document;
use beram\PiggyStatic\MarkupLanguage\Markdawn\Node\Kind\Html\Blockquote;
use beram\PiggyStatic\MarkupLanguage\Markdawn\Node\Kind\Html\Code;
use beram\PiggyStatic\MarkupLanguage\Markdawn\Node\Kind\Html\Comment;
use beram\PiggyStatic\MarkupLanguage\Markdawn\Node\Kind\Html\Emphasis;
use beram\PiggyStatic\MarkupLanguage\Markdawn\Node\Kind\Html\Heading1;
use beram\PiggyStatic\MarkupLanguage\Markdawn\Node\Kind\Html\Heading2;
use beram\PiggyStatic\MarkupLanguage\Markdawn\Node\Kind\Html\Heading3;
use beram\PiggyStatic\MarkupLanguage\Markdawn\Node\Kind\Html\Heading4;
use beram\PiggyStatic\MarkupLanguage\Markdawn\Node\Kind\Html\Heading5;
use beram\PiggyStatic\MarkupLanguage\Markdawn\Node\Kind\Html\Heading6;
use beram\PiggyStatic\MarkupLanguage\Markdawn\Node\Kind\Html\HorizontalRule;
use beram\PiggyStatic\MarkupLanguage\Markdawn\Node\Kind\Html\Html;
use beram\PiggyStatic\MarkupLanguage\Markdawn\Node\Kind\Html\Image;
use beram\PiggyStatic\MarkupLanguage\Markdawn\Node\Kind\Html\Link;
use beram\PiggyStatic\MarkupLanguage\Markdawn\Node\Kind\Html\List_;
use beram\PiggyStatic\MarkupLanguage\Markdawn\Node\Kind\Html\Paragraph;
use beram\PiggyStatic\MarkupLanguage\Markdawn\Node\Kind\Html\Strikethrough;
use beram\PiggyStatic\MarkupLanguage\Markdawn\Node\Kind\Html\Strong;
use beram\PiggyStatic\MarkupLanguage\Markdawn\Node\Kind\Html\Text;
use beram\PiggyStatic\MarkupLanguage\Markdawn\Node\Kind\Metadata\Property;
use PHPUnit\Framework\TestCase;

final class CompilerTest extends TestCase
{
    public function test_compile(): void
    {
        $html = <<<'HTML'
<h1>Title h1</h1>
<h2>Title h2</h2>
<h3>Title h3</h3>
<h4>Title h4</h4>
<h5>Title h5</h5>
<h6>Title h6</h6>
<p>A simple text with some <strong>strong</strong> and <em>emphasis</em> and <s>strikethrough</s>.</p>
<p><strong>Hey <em>hello <s>coucou</s></em></strong></p>
<a href="https://www.google.com">I'm an inline-style link</a><a href="https://www.google.com" title="Google's Homepage">I'm an inline-style link</a><img src="https://placekitten.com/200/300" alt="alt text">
<hr>
<pre><code class="language-php">echo &quot;Hello world!&quot;;</code></pre>
<ul><li>foo</li><li>bar<ul><li>sub-bar</li><li>sand</li></ul>
</li><li>baz</li></ul>
<blockquote><p>a block quote</p>
<p>on multiline</p>
</blockquote>
<!-- This is a comment. Comments are not displayed. -->
<div>
    <p>A test in html</p>
</div>
HTML;

        $nodes = [
            new Heading1([new Text('Title h1')]),
            new Heading2([new Text('Title h2')]),
            new Heading3([new Text('Title h3')]),
            new Heading4([new Text('Title h4')]),
            new Heading5([new Text('Title h5')]),
            new Heading6([new Text('Title h6')]),
            new Paragraph([
                new Text('A simple text with some '),
                new Strong([new Text('strong')]),
                new Text(' and '),
                new Emphasis([new Text('emphasis')]),
                new Text(' and '),
                new Strikethrough([new Text('strikethrough')]),
                new Text('.'),
            ]),
            new Paragraph([
                new Strong([
                    new Text('Hey '),
                    new Emphasis([
                        new Text('hello '),
                        new Strikethrough([new Text('coucou')]),
                    ]),
                ]),
            ]),
            new Link(
                new Link\Text([new Text('I\'m an inline-style link')]),
                new Link\Href('https://www.google.com'),
                null,
            ),
            new Link(
                new Link\Text([new Text('I\'m an inline-style link')]),
                new Link\Href('https://www.google.com'),
                new Link\Title('Google\'s Homepage'),
            ),
            new Image(
              new Image\Src('https://placekitten.com/200/300'),
              new Image\Alt('alt text'),
            ),
            new HorizontalRule(),
            new Code(
                Code\Language::php,
                new Code\Text('echo "Hello world!";'),
            ),
            new List_\Unordered([
                new List_\Unordered\Item([new Text('foo')]),
                new List_\Unordered\Item([
                    new Text('bar'),
                    new List_\Unordered([
                        new List_\Unordered\Item([new Text('sub-bar')]),
                        new List_\Unordered\Item([new Text('sand')]),
                    ]),
                ]),
                new List_\Unordered\Item([new Text('baz')]),
            ]),
            new Blockquote([
                new Paragraph([new Text('a block quote')]),
                new Paragraph([new Text('on multiline')]),
            ]),
            new Comment(' This is a comment. Comments are not displayed. '),
            new Html(<<<'HTML'
<div>
    <p>A test in html</p>
</div>
HTML),
        ];

        $this->assertEquals(
            new Compiled\Document(
                new Compiled\Html($html),
                new Compiled\Metadata([
                    'layout' => new Compiled\Metadata\Property('layout', 'base'),
                    'title' => new Compiled\Metadata\Property('title', 'foo'),
                ]),
            ),
            (new Compiler())->compile(
                new Document(
                    $nodes,
                    [
                        new Property(new Property\Name('layout'), new Property\Value('base')),
                        new Property(new Property\Name('title'), new Property\Value('foo')),
                    ],
                ),
            ),
        );
    }
}
