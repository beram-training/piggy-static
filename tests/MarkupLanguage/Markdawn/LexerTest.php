<?php

declare(strict_types=1);

namespace Tests\beram\PiggyStatic\MarkupLanguage\Markdawn;

use beram\PiggyStatic\MarkupLanguage\Markdawn\Lexer;
use beram\PiggyStatic\MarkupLanguage\Markdawn\Source;
use beram\PiggyStatic\MarkupLanguage\Markdawn\Token;
use beram\PiggyStatic\MarkupLanguage\Markdawn\TokenType;
use PHPUnit\Framework\TestCase;

final class LexerTest extends TestCase
{
    /**
     * @dataProvider tokenizeProvider
     *
     * @param Token[] $expected
     */
    public function test_tokenize(array $expected, Source $source): void
    {
        self::assertEquals($expected, (new Lexer())->tokenize($source));
    }

    public function tokenizeProvider(): \Generator
    {
        yield [
            [
                new Token(TokenType::HEADING1, '#'),
                new Token(TokenType::SPACE, ' '),
                new Token(TokenType::TEXT, 'Title h1'),
                new Token(TokenType::EOL, \PHP_EOL),
                new Token(TokenType::LINE_BLANK, ''),
                new Token(TokenType::HEADING2, '##'),
                new Token(TokenType::SPACE, ' '),
                new Token(TokenType::TEXT, 'Title h2'),
                new Token(TokenType::EOL, \PHP_EOL),
                new Token(TokenType::LINE_BLANK, ''),
                new Token(TokenType::TEXT, 'A simple text.'),
                new Token(TokenType::EOL, \PHP_EOL),
                new Token(TokenType::LINE_BLANK, ''),
                new Token(TokenType::TEXT, 'A simple text on multiline.'),
                new Token(TokenType::EOL, \PHP_EOL),
                new Token(TokenType::TEXT, 'The second line.'),
                new Token(TokenType::EOL, \PHP_EOL),
                new Token(TokenType::LINE_BLANK, ''),
                new Token(TokenType::EOF, ''),
            ],
            Source::fromFilePath(__DIR__.'/fixtures/lexer_simple.md'), ];
        yield [
            [
                new Token(TokenType::HEADING1, '#'),
                new Token(TokenType::SPACE, ' '),
                new Token(TokenType::TEXT, 'Title h1'),
                new Token(TokenType::EOL, \PHP_EOL),
                new Token(TokenType::LINE_BLANK, ''),
                new Token(TokenType::HEADING2, '##'),
                new Token(TokenType::SPACE, ' '),
                new Token(TokenType::TEXT, 'Title h2'),
                new Token(TokenType::EOL, \PHP_EOL),
                new Token(TokenType::LINE_BLANK, ''),
                new Token(TokenType::TEXT, 'A simple text.'),
                new Token(TokenType::EOL, \PHP_EOL),
                new Token(TokenType::LINE_BLANK, ''),
                new Token(TokenType::TEXT, 'A simple text on multiline.'),
                new Token(TokenType::EOL, \PHP_EOL),
                new Token(TokenType::TEXT, 'The second line.'),
                new Token(TokenType::EOL, \PHP_EOL),
                new Token(TokenType::LINE_BLANK, ''),
                new Token(TokenType::TEXT, 'A simple text with some '),
                new Token(TokenType::STRONG, '**'),
                new Token(TokenType::TEXT, 'strong'),
                new Token(TokenType::STRONG, '**'),
                new Token(TokenType::TEXT, '.'),
                new Token(TokenType::EOL, \PHP_EOL),
                new Token(TokenType::LINE_BLANK, ''),
                new Token(TokenType::EOF, ''),
            ],
            Source::fromFilePath(__DIR__.'/fixtures/lexer_simple_strong.md'), ];
        yield [
            [
                new Token(TokenType::METADATA_BEGIN, '@metadata'),
                new Token(TokenType::METADATA_PROPERTY, 'layout'),
                new Token(TokenType::METADATA_OPERATOR_ASSIGNMENT, ':'),
                new Token(TokenType::METADATA_VALUE, 'foo'),
                new Token(TokenType::EOL, \PHP_EOL),
                new Token(TokenType::METADATA_PROPERTY, 'title'),
                new Token(TokenType::METADATA_OPERATOR_ASSIGNMENT, ':'),
                new Token(TokenType::METADATA_VALUE, 'bar'),
                new Token(TokenType::EOL, \PHP_EOL),
                new Token(TokenType::METADATA_END, '@endmetadata'),
                new Token(TokenType::EOL, \PHP_EOL),
                new Token(TokenType::LINE_BLANK, ''),
                new Token(TokenType::HEADING1, '#'),
                new Token(TokenType::SPACE, ' '),
                new Token(TokenType::TEXT, 'Title h1'),
                new Token(TokenType::EOL, \PHP_EOL),
                new Token(TokenType::LINE_BLANK, ''),
                new Token(TokenType::HEADING2, '##'),
                new Token(TokenType::SPACE, ' '),
                new Token(TokenType::TEXT, 'Title h2'),
                new Token(TokenType::EOL, \PHP_EOL),
                new Token(TokenType::LINE_BLANK, ''),
                new Token(TokenType::TEXT, 'A simple text.'),
                new Token(TokenType::EOL, \PHP_EOL),
                new Token(TokenType::LINE_BLANK, ''),
                new Token(TokenType::TEXT, 'A simple text on multiline.'),
                new Token(TokenType::EOL, \PHP_EOL),
                new Token(TokenType::TEXT, 'The second line.'),
                new Token(TokenType::EOL, \PHP_EOL),
                new Token(TokenType::LINE_BLANK, ''),
                new Token(TokenType::TEXT, 'A simple text with some '),
                new Token(TokenType::STRONG, '**'),
                new Token(TokenType::TEXT, 'strong'),
                new Token(TokenType::STRONG, '**'),
                new Token(TokenType::TEXT, ' and '),
                new Token(TokenType::STRONG, '**'),
                new Token(TokenType::TEXT, 'another part strong'),
                new Token(TokenType::STRONG, '**'),
                new Token(TokenType::TEXT, ' with a "normal" ending.'),
                new Token(TokenType::EOL, \PHP_EOL),
                new Token(TokenType::LINE_BLANK, ''),
                new Token(TokenType::EOF, ''),
            ],
            Source::fromFilePath(__DIR__.'/fixtures/lexer.md'), ];
    }
}
