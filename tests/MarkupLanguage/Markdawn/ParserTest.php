<?php

declare(strict_types=1);

namespace Tests\beram\PiggyStatic\MarkupLanguage\Markdawn;

use beram\PiggyStatic\MarkupLanguage\Markdawn\Node\Document;
use beram\PiggyStatic\MarkupLanguage\Markdawn\Node\Kind\Html\Blockquote;
use beram\PiggyStatic\MarkupLanguage\Markdawn\Node\Kind\Html\Code;
use beram\PiggyStatic\MarkupLanguage\Markdawn\Node\Kind\Html\Comment;
use beram\PiggyStatic\MarkupLanguage\Markdawn\Node\Kind\Html\Emphasis;
use beram\PiggyStatic\MarkupLanguage\Markdawn\Node\Kind\Html\Heading1;
use beram\PiggyStatic\MarkupLanguage\Markdawn\Node\Kind\Html\Heading2;
use beram\PiggyStatic\MarkupLanguage\Markdawn\Node\Kind\Html\Heading3;
use beram\PiggyStatic\MarkupLanguage\Markdawn\Node\Kind\Html\Heading4;
use beram\PiggyStatic\MarkupLanguage\Markdawn\Node\Kind\Html\Heading5;
use beram\PiggyStatic\MarkupLanguage\Markdawn\Node\Kind\Html\Heading6;
use beram\PiggyStatic\MarkupLanguage\Markdawn\Node\Kind\Html\HorizontalRule;
use beram\PiggyStatic\MarkupLanguage\Markdawn\Node\Kind\Html\Html;
use beram\PiggyStatic\MarkupLanguage\Markdawn\Node\Kind\Html\Image;
use beram\PiggyStatic\MarkupLanguage\Markdawn\Node\Kind\Html\Link;
use beram\PiggyStatic\MarkupLanguage\Markdawn\Node\Kind\Html\Paragraph;
use beram\PiggyStatic\MarkupLanguage\Markdawn\Node\Kind\Html\Strikethrough;
use beram\PiggyStatic\MarkupLanguage\Markdawn\Node\Kind\Html\Strong;
use beram\PiggyStatic\MarkupLanguage\Markdawn\Node\Kind\Html\Text;
use beram\PiggyStatic\MarkupLanguage\Markdawn\Node\Kind\Metadata\Property;
use beram\PiggyStatic\MarkupLanguage\Markdawn\Parser;
use beram\PiggyStatic\MarkupLanguage\Markdawn\Token;
use beram\PiggyStatic\MarkupLanguage\Markdawn\TokenType;
use PHPUnit\Framework\TestCase;

final class ParserTest extends TestCase
{
    /**
     * @dataProvider parseProvider
     */
    public function test_parse(Document $expected, array $tokens): void
    {
        self::assertEquals($expected, (new Parser())->parse($tokens));
    }

    public function parseProvider(): \Generator
    {
        yield [
            new Document([new Heading1([new Text('Title h1')])], []),
            [
                new Token(
                    TokenType::HEADING1,
                    '#',
                ),
                new Token(
                    TokenType::SPACE,
                    ' ',
                ),
                new Token(
                    TokenType::TEXT,
                    'Title h1',
                ),
                new Token(
                    TokenType::EOL,
                    '\n',
                ),
                new Token(
                    TokenType::EOF,
                    '',
                ),
            ],
        ];
        yield [
            new Document(
                [
                    new Heading1([new Text('Title h1')]),
                    new Heading2([new Text('Title h2')]),
                    new Heading3([new Text('Title h3')]),
                    new Heading4([new Text('Title h4')]),
                    new Heading5([new Text('Title h5')]),
                    new Heading6([new Text('Title h6')]),
                ],
                []
            ),
            [
                new Token(
                    TokenType::HEADING1,
                    '#',
                ),
                new Token(
                    TokenType::SPACE,
                    ' ',
                ),
                new Token(
                    TokenType::TEXT,
                    'Title h1',
                ),
                new Token(
                    TokenType::EOL,
                    '\n',
                ),
                new Token(
                    TokenType::HEADING2,
                    '#',
                ),
                new Token(
                    TokenType::SPACE,
                    ' ',
                ),
                new Token(
                    TokenType::TEXT,
                    'Title h2',
                ),
                new Token(
                    TokenType::EOL,
                    '\n',
                ),
                new Token(
                    TokenType::HEADING3,
                    '#',
                ),
                new Token(
                    TokenType::SPACE,
                    ' ',
                ),
                new Token(
                    TokenType::TEXT,
                    'Title h3',
                ),
                new Token(
                    TokenType::EOL,
                    '\n',
                ),
                new Token(
                    TokenType::HEADING4,
                    '#',
                ),
                new Token(
                    TokenType::SPACE,
                    ' ',
                ),
                new Token(
                    TokenType::TEXT,
                    'Title h4',
                ),
                new Token(
                    TokenType::EOL,
                    '\n',
                ),
                new Token(
                    TokenType::HEADING5,
                    '#',
                ),
                new Token(
                    TokenType::SPACE,
                    ' ',
                ),
                new Token(
                    TokenType::TEXT,
                    'Title h5',
                ),
                new Token(
                    TokenType::EOL,
                    '\n',
                ),
                new Token(
                    TokenType::HEADING6,
                    '#',
                ),
                new Token(
                    TokenType::SPACE,
                    ' ',
                ),
                new Token(
                    TokenType::TEXT,
                    'Title h6',
                ),
                new Token(
                    TokenType::EOL,
                    '\n',
                ),
                new Token(
                    TokenType::EOF,
                    '',
                ),
            ],
        ];
        yield [
            new Document(
                [
                    new Heading1([new Text('Title h1')]),
                    new Paragraph([
                        new Text('A simple text on multiline.'),
                        new Text(' '),
                        new Text('The second line.'),
                    ]),
                ],
                []
            ),
            [
                new Token(
                    TokenType::HEADING1,
                    '#',
                ),
                new Token(
                    TokenType::SPACE,
                    ' ',
                ),
                new Token(
                    TokenType::TEXT,
                    'Title h1',
                ),
                new Token(
                    TokenType::EOL,
                    '\n',
                ),
                new Token(
                    TokenType::LINE_BLANK,
                    '\n',
                ),
                new Token(
                    TokenType::TEXT,
                    'A simple text on multiline.',
                ),
                new Token(
                    TokenType::EOL,
                    '\n',
                ),
                new Token(
                    TokenType::TEXT,
                    'The second line.',
                ),
                new Token(
                    TokenType::EOL,
                    '\n',
                ),
                new Token(
                    TokenType::LINE_BLANK,
                    '\n',
                ),
                new Token(
                    TokenType::EOF,
                    '',
                ),
            ],
        ];
        yield [
            new Document(
                [
                    new Heading1([new Text('Title h1')]),
                    new Paragraph([
                        new Text('A simple text with some '),
                        new Strong([new Text('strong')]),
                        new Text('.'),
                    ]),
                ],
                [],
            ),
            [
                new Token(
                    TokenType::HEADING1,
                    '#',
                ),
                new Token(
                    TokenType::SPACE,
                    ' ',
                ),
                new Token(
                    TokenType::TEXT,
                    'Title h1',
                ),
                new Token(
                    TokenType::EOL,
                    '\n',
                ),
                new Token(
                    TokenType::LINE_BLANK,
                    '\n',
                ),
                new Token(
                    TokenType::TEXT,
                    'A simple text with some ',
                ),
                new Token(
                    TokenType::STRONG,
                    '**',
                ),
                new Token(
                    TokenType::TEXT,
                    'strong',
                ),
                new Token(
                    TokenType::STRONG,
                    '**',
                ),
                new Token(
                    TokenType::TEXT,
                    '.',
                ),
                new Token(
                    TokenType::EOL,
                    '\n',
                ),
                new Token(
                    TokenType::LINE_BLANK,
                    '\n',
                ),
                new Token(
                    TokenType::EOF,
                    '',
                ),
            ],
        ];
        yield [
            new Document(
                [
                    new Paragraph([
                        new Strong([
                            new Text('Hey coucou!'),
                        ]),
                        new Text(' Hey you! '),
                        new Emphasis([new Text('nice!')]),
                    ]),
                ],
                [],
            ),
            [
                new Token(
                    TokenType::LINE_BLANK,
                    '\n',
                ),
                new Token(
                    TokenType::STRONG,
                    '**',
                ),
                new Token(
                    TokenType::TEXT,
                    'Hey coucou!',
                ),
                new Token(
                    TokenType::STRONG,
                    '**',
                ),
                new Token(
                    TokenType::TEXT,
                    ' Hey you! ',
                ),
                new Token(
                    TokenType::EMPHASIS,
                    '*',
                ),
                new Token(
                    TokenType::TEXT,
                    'nice!',
                ),
                new Token(
                    TokenType::EMPHASIS,
                    '*',
                ),
                new Token(
                    TokenType::EOL,
                    '\n',
                ),
                new Token(
                    TokenType::LINE_BLANK,
                    '\n',
                ),
                new Token(
                    TokenType::EOF,
                    '',
                ),
            ],
        ];
        yield [
            new Document(
                [
                    new Heading1([new Text('Title h1')]),
                    new Paragraph([
                        new Text('A simple text with some '),
                        new Strong([
                            new Text('strong'),
                            new Emphasis([new Text('emphasis')]),
                        ]),
                        new Emphasis([new Text('emphasis')]),
                        new Text('.'),
                    ]),
                ],
                [],
            ),
            [
                new Token(
                    TokenType::HEADING1,
                    '#',
                ),
                new Token(
                    TokenType::SPACE,
                    ' ',
                ),
                new Token(
                    TokenType::TEXT,
                    'Title h1',
                ),
                new Token(
                    TokenType::EOL,
                    '\n',
                ),
                new Token(
                    TokenType::LINE_BLANK,
                    '\n',
                ),
                new Token(
                    TokenType::TEXT,
                    'A simple text with some ',
                ),
                new Token(
                    TokenType::STRONG,
                    '**',
                ),
                new Token(
                    TokenType::TEXT,
                    'strong',
                ),
                new Token(
                    TokenType::EMPHASIS,
                    '*',
                ),
                new Token(
                    TokenType::TEXT,
                    'emphasis',
                ),
                new Token(
                    TokenType::EMPHASIS,
                    '*',
                ),
                new Token(
                    TokenType::STRONG,
                    '**',
                ),
                new Token(
                    TokenType::EMPHASIS,
                    '*',
                ),
                new Token(
                    TokenType::TEXT,
                    'emphasis',
                ),
                new Token(
                    TokenType::EMPHASIS,
                    '*',
                ),
                new Token(
                    TokenType::TEXT,
                    '.',
                ),
                new Token(
                    TokenType::EOL,
                    '\n',
                ),
                new Token(
                    TokenType::LINE_BLANK,
                    '\n',
                ),
                new Token(
                    TokenType::EOF,
                    '',
                ),
            ],
        ];
        yield [
            new Document(
                [
                    new Heading1([new Text('Title h1')]),
                    new Paragraph([
                        new Text('A simple text with some '),
                        new Strong([
                            new Text('strong'),
                            new Emphasis([new Text('emphasis')]),
                            new Text(' '),
                            new Strikethrough([new Text('strikethrough')]),
                        ]),
                        new Emphasis([new Text('emphasis')]),
                        new Text('.'),
                    ]),
                ],
                [],
            ),
            [
                new Token(
                    TokenType::HEADING1,
                    '#',
                ),
                new Token(
                    TokenType::SPACE,
                    ' ',
                ),
                new Token(
                    TokenType::TEXT,
                    'Title h1',
                ),
                new Token(
                    TokenType::EOL,
                    '\n',
                ),
                new Token(
                    TokenType::LINE_BLANK,
                    '\n',
                ),
                new Token(
                    TokenType::TEXT,
                    'A simple text with some ',
                ),
                new Token(
                    TokenType::STRONG,
                    '**',
                ),
                new Token(
                    TokenType::TEXT,
                    'strong',
                ),
                new Token(
                    TokenType::EMPHASIS,
                    '*',
                ),
                new Token(
                    TokenType::TEXT,
                    'emphasis',
                ),
                new Token(
                    TokenType::EMPHASIS,
                    '*',
                ),
                new Token(
                    TokenType::TEXT,
                    ' ',
                ),
                new Token(
                    TokenType::STRIKETHROUGH,
                    '~~',
                ),
                new Token(
                    TokenType::TEXT,
                    'strikethrough',
                ),
                new Token(
                    TokenType::STRIKETHROUGH,
                    '~~',
                ),
                new Token(
                    TokenType::STRONG,
                    '**',
                ),
                new Token(
                    TokenType::EMPHASIS,
                    '*',
                ),
                new Token(
                    TokenType::TEXT,
                    'emphasis',
                ),
                new Token(
                    TokenType::EMPHASIS,
                    '*',
                ),
                new Token(
                    TokenType::TEXT,
                    '.',
                ),
                new Token(
                    TokenType::EOL,
                    '\n',
                ),
                new Token(
                    TokenType::LINE_BLANK,
                    '\n',
                ),
                new Token(
                    TokenType::EOF,
                    '',
                ),
            ],
        ];
        yield [
            new Document(
                [
                    new Paragraph([
                        new Strong([
                            new Text('Hey coucou!'),
                            new Emphasis([
                                new Text('nice!'),
                                new Strikethrough([new Text('A strikethrough text')]),
                            ]),
                            new Text(' Hey you! '),
                        ]),
                    ]),
                ],
                [],
            ),
            [
                new Token(
                    TokenType::LINE_BLANK,
                    '\n',
                ),
                new Token(
                    TokenType::STRONG,
                    '**',
                ),
                new Token(
                    TokenType::TEXT,
                    'Hey coucou!',
                ),
                new Token(
                    TokenType::EMPHASIS,
                    '*',
                ),
                new Token(
                    TokenType::TEXT,
                    'nice!',
                ),
                new Token(
                    TokenType::STRIKETHROUGH,
                    '~~',
                ),
                new Token(
                    TokenType::TEXT,
                    'A strikethrough text',
                ),
                new Token(
                    TokenType::STRIKETHROUGH,
                    '~~',
                ),
                new Token(
                    TokenType::EMPHASIS,
                    '*',
                ),
                new Token(
                    TokenType::TEXT,
                    ' Hey you! ',
                ),
                new Token(
                    TokenType::STRONG,
                    '**',
                ),
                new Token(
                    TokenType::EOL,
                    '\n',
                ),
                new Token(
                    TokenType::LINE_BLANK,
                    '\n',
                ),
                new Token(
                    TokenType::EOF,
                    '',
                ),
            ],
        ];
        yield [
            new Document(
                [
                    new Heading1([new Text('Title h1')]),
                    new Paragraph([
                        new Text('A simple text with some '),
                        new Strong([new Text('strong')]),
                        new Text(' and '),
                        new Emphasis([new Text('emphasis')]),
                        new Text(' and '),
                        new Strikethrough([new Text('strikethrough')]),
                        new Text('.'),
                    ]),
                ],
                [],
            ),
            [
                new Token(
                    TokenType::HEADING1,
                    '#',
                ),
                new Token(
                    TokenType::SPACE,
                    ' ',
                ),
                new Token(
                    TokenType::TEXT,
                    'Title h1',
                ),
                new Token(
                    TokenType::EOL,
                    '\n',
                ),
                new Token(
                    TokenType::LINE_BLANK,
                    '',
                ),
                new Token(
                    TokenType::TEXT,
                    'A simple text with some ',
                ),
                new Token(
                    TokenType::STRONG,
                    '**',
                ),
                new Token(
                    TokenType::TEXT,
                    'strong',
                ),
                new Token(
                    TokenType::STRONG,
                    '**',
                ),
                new Token(
                    TokenType::TEXT,
                    ' and ',
                ),
                new Token(
                    TokenType::EMPHASIS,
                    '*',
                ),
                new Token(
                    TokenType::TEXT,
                    'emphasis',
                ),
                new Token(
                    TokenType::EMPHASIS,
                    '*',
                ),
                new Token(
                    TokenType::TEXT,
                    ' and ',
                ),
                new Token(
                    TokenType::STRIKETHROUGH,
                    '~~',
                ),
                new Token(
                    TokenType::TEXT,
                    'strikethrough',
                ),
                new Token(
                    TokenType::STRIKETHROUGH,
                    '~~',
                ),
                new Token(
                    TokenType::TEXT,
                    '.',
                ),
                new Token(
                    TokenType::EOL,
                    '\n',
                ),
                new Token(
                    TokenType::LINE_BLANK,
                    '\n',
                ),
                new Token(
                    TokenType::EOF,
                    '',
                ),
            ],
        ];
        yield [
            new Document(
                [
                    new Image(
                        new Image\Src('https://placekitten.com/200/300'),
                        new Image\Alt('alt text'),
                    ),
                ],
                [],
            ),
            [
                new Token(
                    TokenType::IMAGE,
                    '![alt text](https://placekitten.com/200/300)',
                ),
                new Token(
                    TokenType::EOL,
                    '\n',
                ),
                new Token(
                    TokenType::EOF,
                    '',
                ),
            ],
        ];
        yield [
            new Document(
                [
                    new Heading1([new Text('Title h1')]),
                    new Paragraph([
                        new Text('A simple text with some '),
                        new Strong([new Text('strong')]),
                        new Text('. '),
                        new Image(
                            new Image\Src('https://placekitten.com/200/300'),
                            new Image\Alt('alt text'),
                        ),
                    ]),
                ],
                [],
            ),
            [
                new Token(
                    TokenType::HEADING1,
                    '#',
                ),
                new Token(
                    TokenType::SPACE,
                    ' ',
                ),
                new Token(
                    TokenType::TEXT,
                    'Title h1',
                ),
                new Token(
                    TokenType::EOL,
                    '\n',
                ),
                new Token(
                    TokenType::LINE_BLANK,
                    '\n',
                ),
                new Token(
                    TokenType::TEXT,
                    'A simple text with some ',
                ),
                new Token(
                    TokenType::STRONG,
                    '**',
                ),
                new Token(
                    TokenType::TEXT,
                    'strong',
                ),
                new Token(
                    TokenType::STRONG,
                    '**',
                ),
                new Token(
                    TokenType::TEXT,
                    '. ',
                ),
                new Token(
                    TokenType::IMAGE,
                    '![alt text](https://placekitten.com/200/300)',
                ),
                new Token(
                    TokenType::EOL,
                    '\n',
                ),
                new Token(
                    TokenType::LINE_BLANK,
                    '\n',
                ),
                new Token(
                    TokenType::EOF,
                    '',
                ),
            ],
        ];
        yield [
            new Document(
                [
                    new Link(
                        new Link\Text([new Text('I\'m an inline-style link')]),
                        new Link\Href('https://www.google.com'),
                        null,
                    ),
                    new Link(
                        new Link\Text([new Text('I\'m an inline-style link with title')]),
                        new Link\Href('https://www.google.com'),
                        new Link\Title('Google\'s Homepage'),
                    ),
                ],
                [],
            ),
            [
                new Token(
                    TokenType::LINK,
                    '[I\'m an inline-style link](https://www.google.com)',
                ),
                new Token(
                    TokenType::LINK,
                    '[I\'m an inline-style link with title](https://www.google.com "Google\'s Homepage")',
                ),
                new Token(
                    TokenType::EOL,
                    '\n',
                ),
                new Token(
                    TokenType::EOF,
                    '',
                ),
            ],
        ];
        yield [
            new Document(
                [
                    new Heading1([new Text('Title h1')]),
                    new HorizontalRule(),
                    new Paragraph([
                        new Text('A simple text with some '),
                        new Strong([new Text('strong')]),
                        new Text('. '),
                        new Link(
                            new Link\Text([new Text('I\'m an inline-style link')]),
                            new Link\Href('https://www.google.com'),
                            null,
                        ),
                        new Text('And also '),
                        new Link(
                            new Link\Text([new Text('I\'m an inline-style link with title')]),
                            new Link\Href('https://www.google.com'),
                            new Link\Title('Google\'s Homepage'),
                        ),
                    ]),
                ],
                [],
            ),
            [
                new Token(
                    TokenType::HEADING1,
                    '#',
                ),
                new Token(
                    TokenType::SPACE,
                    ' ',
                ),
                new Token(
                    TokenType::TEXT,
                    'Title h1',
                ),
                new Token(
                    TokenType::EOL,
                    '\n',
                ),
                new Token(
                    TokenType::LINE_BLANK,
                    '\n',
                ),
                new Token(
                    TokenType::HORIZONTAL_RULE,
                    '---',
                ),
                new Token(
                    TokenType::LINE_BLANK,
                    '\n',
                ),
                new Token(
                    TokenType::TEXT,
                    'A simple text with some ',
                ),
                new Token(
                    TokenType::STRONG,
                    '**',
                ),
                new Token(
                    TokenType::TEXT,
                    'strong',
                ),
                new Token(
                    TokenType::STRONG,
                    '**',
                ),
                new Token(
                    TokenType::TEXT,
                    '. ',
                ),
                new Token(
                    TokenType::LINK,
                    '[I\'m an inline-style link](https://www.google.com)',
                ),
                new Token(
                    TokenType::TEXT,
                    'And also ',
                ),
                new Token(
                    TokenType::LINK,
                    '[I\'m an inline-style link with title](https://www.google.com "Google\'s Homepage")',
                ),
                new Token(
                    TokenType::EOL,
                    '\n',
                ),
                new Token(
                    TokenType::LINE_BLANK,
                    '\n',
                ),
                new Token(
                    TokenType::EOF,
                    '',
                ),
            ],
        ];
        yield [
            new Document(
                [
                    new Code(
                        Code\Language::php,
                        new Code\Text('echo "Hello world;"'),
                    ),
                    new Code(
                        Code\Language::none,
                        new Code\Text('we do not know'),
                    ),
                ],
                [],
            ),
            [
                new Token(
                    TokenType::CODE_BEGIN,
                    '```php',
                ),
                new Token(
                    TokenType::TEXT,
                    'echo "Hello world;"',
                ),
                new Token(
                    TokenType::CODE_END,
                    '```',
                ),
                new Token(
                    TokenType::LINE_BLANK,
                    '\n',
                ),
                new Token(
                    TokenType::CODE_BEGIN,
                    '```',
                ),
                new Token(
                    TokenType::TEXT,
                    'we do not know',
                ),
                new Token(
                    TokenType::CODE_END,
                    '```',
                ),
                new Token(
                    TokenType::EOF,
                    '',
                ),
            ],
        ];
        yield [
            new Document(
                [
                    new Blockquote([
                        new Paragraph([
                            new Text('A simple text with some '),
                            new Strong([new Text('strong')]),
                            new Text('.'),
                        ]),
                        new Paragraph([
                            new Text('Another paragraph inside.'),
                        ]),
                    ]),
                ],
                [],
            ),
            [
                new Token(
                    TokenType::BLOCKQUOTE_BEGIN,
                    '>',
                ),
                new Token(
                    TokenType::TEXT,
                    'A simple text with some ',
                ),
                new Token(
                    TokenType::STRONG,
                    '**',
                ),
                new Token(
                    TokenType::TEXT,
                    'strong',
                ),
                new Token(
                    TokenType::STRONG,
                    '**',
                ),
                new Token(
                    TokenType::TEXT,
                    '.',
                ),
                new Token(
                    TokenType::LINE_BLANK,
                    '',
                ),
                new Token(
                    TokenType::TEXT,
                    'Another paragraph inside.',
                ),
                new Token(
                    TokenType::BLOCKQUOTE_END,
                    '',
                ),
                new Token(
                    TokenType::EOL,
                    '\n',
                ),
                new Token(
                    TokenType::LINE_BLANK,
                    '\n',
                ),
                new Token(
                    TokenType::EOF,
                    '',
                ),
            ],
        ];
        yield [
            new Document(
                [
                    new Comment('a comment'),
                ],
                [],
            ),
            [
                new Token(
                    TokenType::COMMENT_BEGIN,
                    '<!--',
                ),
                new Token(
                    TokenType::TEXT,
                    'a comment',
                ),
                new Token(
                    TokenType::COMMENT_END,
                    '-->',
                ),
                new Token(
                    TokenType::EOL,
                    '\n',
                ),
                new Token(
                    TokenType::LINE_BLANK,
                    '\n',
                ),
                new Token(
                    TokenType::EOF,
                    '',
                ),
            ],
        ];
        yield [
            new Document([new Html('some HTML')], []),
            [
                new Token(
                    TokenType::HTML_BEGIN,
                    '',
                ),
                new Token(
                    TokenType::TEXT,
                    'some HTML',
                ),
                new Token(
                    TokenType::HTML_END,
                    '',
                ),
                new Token(
                    TokenType::EOL,
                    '\n',
                ),
                new Token(
                    TokenType::LINE_BLANK,
                    '\n',
                ),
                new Token(
                    TokenType::EOF,
                    '',
                ),
            ],
        ];
        yield [
            new Document(
                [new Heading1([new Text('Title h1')])],
                [
                    new Property(new Property\Name('layout'), new Property\Value('base')),
                    new Property(new Property\Name('title'), new Property\Value('foo')),
                ],
            ),
            [
                new Token(TokenType::METADATA_BEGIN, '@metadata'),
                new Token(TokenType::METADATA_PROPERTY, 'layout'),
                new Token(TokenType::METADATA_OPERATOR_ASSIGNMENT, ':'),
                new Token(TokenType::METADATA_VALUE, 'base'),
                new Token(TokenType::EOL, \PHP_EOL),
                new Token(TokenType::METADATA_PROPERTY, 'title'),
                new Token(TokenType::METADATA_OPERATOR_ASSIGNMENT, ':'),
                new Token(TokenType::METADATA_VALUE, 'foo'),
                new Token(TokenType::EOL, \PHP_EOL),
                new Token(TokenType::METADATA_END, '@endmetadata'),
                new Token(TokenType::EOL, \PHP_EOL),
                new Token(TokenType::LINE_BLANK, ''),
                new Token(TokenType::HEADING1, '#'),
                new Token(TokenType::SPACE, ' '),
                new Token(TokenType::TEXT, 'Title h1'),
                new Token(TokenType::EOL, \PHP_EOL),
                new Token(TokenType::LINE_BLANK, ''),
                new Token(TokenType::EOF, ''),
            ],
        ];
    }
}
