@metadata
layout: foo
title: bar
@endmetadata

# Title h1

## Title h2

### Title h3

#### Title h4

##### Title h5

###### Title h6

A simple text.

A simple text on multiline.
The second line.

A simple text with some **strong** and **another part strong** with a "normal" ending.

---

**Hey!** You are *lucky* to have fun ~~here~~!

![alt text](https://placekitten.com/200/300)

Do inline images works? ![does it?](https://placekitten.com/50/50)

Look at it [I'm an inline-style link](https://www.google.com)

Here's too [I'm an inline-style link with title](https://www.google.com "Google's Homepage") duh :P

```php
echo "Hello world!";
```

```php
echo 'foo';
echo 'bar';
```

```php
final class Foo {
    public function __construct(
        public readonly string $name,
    ) {
    }
}
```

> a block quote
>
> on multiline

> a single line

@html
<div>
    <p>A test in html</p>
</div>
@endhtml
