# Title h1

## Title h2

A simple text.

A simple text on multiline.
The second line.

A simple text with some **strong** and **another part strong** with a "normal" ending.

**Hey!** You are *lucky* to have fun ~~here~~!

![alt text](https://placekitten.com/200/300)

Look at it [I'm an inline-style link](https://www.google.com)

Here's too [I'm an inline-style link with title](https://www.google.com "Google's Homepage") duh :P
