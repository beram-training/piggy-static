@metadata
layout: foo
title: bar
@endmetadata

# Title h1

## Title h2

### Title h3

#### Title h4

##### Title h5

###### Title h6

A simple text.

A simple text with some **strong** and *emphasis* and ~~strikethrough~~.

A simple text on multiline.
The second line.

[I'm an inline-style link](https://www.google.com)

[I'm an inline-style link with title](https://www.google.com "Google's Homepage")

![alt text](https://placekitten.com/200/300)

---

```php
echo "Hello world!";
```

* foo
* bar
  * sub-bar
  * sand
* baz

> a block quote
>
> on multiline

> a single line

<!-- This is a comment. Comments are not displayed. -->

@html
<div>
    <p>A test in html</p>
</div>
@endhtml
