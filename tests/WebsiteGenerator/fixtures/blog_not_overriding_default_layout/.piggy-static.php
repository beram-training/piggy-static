<?php

use beram\PiggyStatic\WebsiteGenerator\Config;

return Config::default()
    ->withSrc(__DIR__)
    ->withDest(__DIR__.'/build')
    ;
