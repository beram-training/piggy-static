<?php

use beram\PiggyStatic\WebsiteGenerator\Config;
use beram\PiggyStatic\WebsiteGenerator\Layout;

return Config::default()
    ->withSrc(__DIR__)
    ->withDest(__DIR__.'/build')
    ->withLayout('default', new Layout(__DIR__.'/_layouts/default.html'))
    ->withLayout('homepage', new Layout(__DIR__.'/_layouts/homepage.html'))
    ->withLayout('article', new Layout(__DIR__.'/_layouts/article.html'))
    ;
