<?php

declare(strict_types=1);

namespace Tests\beram\PiggyStatic\WebsiteGenerator\Command;

use beram\PiggyStatic\CommandLine\ExitCode;
use beram\PiggyStatic\CommandLine\Input;
use beram\PiggyStatic\CommandLine\Test\CommandTester;
use beram\PiggyStatic\CommandLine\Test\TestOutput;
use beram\PiggyStatic\FileSystem\FileSystem;
use beram\PiggyStatic\Kernel;
use beram\PiggyStatic\WebsiteGenerator\Command\Generate;
use PHPUnit\Framework\TestCase;

final class GenerateTest extends TestCase
{
    private const FIXTURES_BUILD_DIRECTORY = __DIR__.'/../fixtures/blog/build';
    private const EXPECTED_BUILD_DIRECTORY = __DIR__.'/../fixtures/blog_expected';
    private readonly Generate $command;
    private readonly CommandTester $commandTester;

    protected function setUp(): void
    {
        $this->cleanBuiltFixtures();

        $kernel = new Kernel();
        $this->command = $kernel->container->get(Generate::class);
        $this->commandTester = new CommandTester();
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        $this->cleanBuiltFixtures();
    }

    public function test_it_generates_a_blog(): void
    {
        $output = new TestOutput();
        $exitCode = $this->commandTester->execute($this->command, new Input(new Input\Options(['config' => __DIR__.'/../fixtures/blog/.piggy-static.php'])), $output);

        self::assertSame(ExitCode::OK, $exitCode);
        self::assertSame([], $output->getOut());
        self::assertSame([], $output->getError());

        self::assertDirectoryExists(self::FIXTURES_BUILD_DIRECTORY);
        self::assertDirectoryExists(self::FIXTURES_BUILD_DIRECTORY.'/assets');
        self::assertDirectoryDoesNotExist(self::FIXTURES_BUILD_DIRECTORY.'/_layouts');
        foreach (['/index.html', '/about.html', '/articles/first.html', '/assets/css/main.css'] as $file) {
            self::assertFileEquals(self::EXPECTED_BUILD_DIRECTORY.$file, self::FIXTURES_BUILD_DIRECTORY.$file);
        }
    }

    public function test_it_generates_a_blog_not_overriding_default_layout(): void
    {
        $output = new TestOutput();
        $exitCode = $this->commandTester->execute($this->command, new Input(new Input\Options(['config' => __DIR__.'/../fixtures/blog_not_overriding_default_layout/.piggy-static.php'])), $output);

        self::assertSame(ExitCode::OK, $exitCode);
        self::assertSame([], $output->getOut());
        self::assertSame([], $output->getError());

        self::assertDirectoryExists(__DIR__.'/../fixtures/blog_not_overriding_default_layout/build');
        self::assertFileEquals(__DIR__.'/../fixtures/blog_not_overriding_default_layout_expected/index.html', __DIR__.'/../fixtures/blog_not_overriding_default_layout/build/index.html');
    }

    private function cleanBuiltFixtures(): void
    {
        FileSystem::delete(self::FIXTURES_BUILD_DIRECTORY);
        FileSystem::delete(__DIR__.'/../fixtures/blog_not_overriding_default_layout/build');
    }
}
