<?php

declare(strict_types=1);

\spl_autoload_register(function (string $class): void {
    $classFile = \str_replace(
        ['beram\\PiggyStatic\\', '\\'],
        ['', '/'],
        $class
    );
    $path = __DIR__ . '/src/' . $classFile . '.php';
    if (\file_exists($path)) {
        include $path;
    }
});
