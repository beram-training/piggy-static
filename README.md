# Piggy Static

An opinionated static site generator.

**PLEASE DON'T USE THIS**

## Requirements

- PHP 8.1

And that's it!

Having no dependency was a goal (only for fun).

## Install

Create a config file `.piggy-static.php` similar to:

```php
<?php

use beram\PiggyStatic\WebsiteGenerator\Config;
use beram\PiggyStatic\WebsiteGenerator\Layout;

return Config::default()
    ->withSrc(__DIR__.'/src')
    ->withDest(__DIR__.'/build')
    ->withLayout('default', new Layout(__DIR__.'/src/_layouts/default.html'))
    ->withLayout('article', new Layout(__DIR__.'/src/_layouts/article.html'))
    ;
```

## Usage

If you have a `.piggy-static.php` config file where you run the command:

```bash
php bin/generate.php
```

Otherwise:

```bash
php bin/generate.php --config path/to/config.php
```

## Contributing

See the [contributing guide](CONTRIBUTING.md).
