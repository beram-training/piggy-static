<?php

declare(strict_types=1);

namespace beram\PiggyStatic\FileSystem;

use beram\PiggyStatic\FileSystem\Finder\FilterByFilenameRecursiveIterator;

final class Finder
{
    public function __construct(
        public readonly string $path,
        public readonly array $exclude,
    ) {
    }

    public static function create(): self
    {
        return new self('', []);
    }

    public function in(string $path): self
    {
        return new self(
            $path,
            $this->exclude
        );
    }

    public function exclude(string|array $files): self
    {
        $exclude = $this->exclude;

        return new self(
            $this->path,
            \array_merge($exclude, (array) $files),
        );
    }

    public function find(): \Iterator
    {
        return new \RecursiveIteratorIterator(
            new FilterByFilenameRecursiveIterator(
                new \RecursiveDirectoryIterator(
                    $this->path,
                    \FilesystemIterator::KEY_AS_PATHNAME | \FilesystemIterator::CURRENT_AS_FILEINFO | \FilesystemIterator::SKIP_DOTS
                ),
                $this->exclude
            ),
        );
    }
}
