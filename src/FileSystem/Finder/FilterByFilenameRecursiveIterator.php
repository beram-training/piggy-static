<?php

declare(strict_types=1);

namespace beram\PiggyStatic\FileSystem\Finder;

use RecursiveIterator;

final class FilterByFilenameRecursiveIterator extends \FilterIterator implements \RecursiveIterator
{
    public function __construct(
        private readonly RecursiveIterator $iterator,
        private readonly array $files,
    ) {
        parent::__construct($iterator);
    }

    public function accept(): bool
    {
        /** @psalm-suppress MixedMethodCall */
        return false === \in_array($this->current()->getFilename(), $this->files, true);
    }

    public function hasChildren(): bool
    {
        return $this->getInnerIterator()->hasChildren();
    }

    public function getChildren(): ?RecursiveIterator
    {
        return new self($this->getInnerIterator()->getChildren(), $this->files);
    }

    public function getInnerIterator(): RecursiveIterator
    {
        return $this->iterator;
    }
}
