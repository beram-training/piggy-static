<?php

declare(strict_types=1);

namespace beram\PiggyStatic\FileSystem\Exception;

final class FileSystemException extends \RuntimeException
{
}
