<?php

declare(strict_types=1);

namespace beram\PiggyStatic\FileSystem;

use beram\PiggyStatic\FileSystem\Exception\FileSystemException;

final class FileSystem
{
    public static function createDir(string $dir, int $mode = 0777): void
    {
        if (!\is_dir($dir) && !@\mkdir($dir, $mode, true) && !\is_dir($dir)) {
            throw new FileSystemException(\sprintf('Unable to create directory "%s" with mode %s.', \realpath($dir), \decoct($mode)));
        }
    }

    public static function copy(string $src, string $dest): void
    {
        if (\stream_is_local($src) && !\file_exists($src)) {
            throw new FileSystemException(\sprintf("File or directory '%s' not found.", \realpath($src)));
        }

        if (\is_dir($src)) {
            static::createDir($dest);
            /** @var \SplFileInfo $file */
            foreach (new \FilesystemIterator($dest) as $file) {
                static::delete($file->getPathname());
            }

            $files = new \RecursiveIteratorIterator(
                new \RecursiveDirectoryIterator(
                    $src,
                    \RecursiveDirectoryIterator::SKIP_DOTS
                ),
                \RecursiveIteratorIterator::SELF_FIRST
            );
            /** @var \SplFileInfo $file */
            foreach ($files as $file) {
                $path = \sprintf('%s/%s', $dest, $files->getSubPathName());

                if ($file->isDir()) {
                    static::createDir($path);
                    continue;
                }

                static::copy($file->getPathname(), $path);
            }

            return;
        }

        static::createDir(\dirname($dest));
        if (false === ($srcStream = @\fopen($src, 'r'))) {
            throw new FileSystemException(\sprintf('Unable to copy file "%s" to "%s": cannot open src stream.', \realpath($src), \realpath($dest)));
        }
        if (false === ($destStream = @\fopen($dest, 'w'))) {
            throw new FileSystemException(\sprintf('Unable to copy file "%s" to "%s": cannot open dest stream.', \realpath($src), \realpath($dest)));
        }
        if (false === @\stream_copy_to_stream($srcStream, $destStream)) {
            throw new FileSystemException(\sprintf('Unable to copy file "%s" to "%s".', \realpath($src), \realpath($dest)));
        }
    }

    public static function delete(string $path): void
    {
        if (\is_file($path) || \is_link($path)) {
            if (!@\unlink($path)) {
                throw new FileSystemException(\sprintf('Unable to delete "%s".', \realpath($path)));
            }

            return;
        }

        if (\is_dir($path)) {
            /** @var \SplFileInfo $file */
            foreach (new \FilesystemIterator($path) as $file) {
                static::delete($file->getPathname());
            }

            if (!@\rmdir($path)) {
                throw new FileSystemException(\sprintf('Unable to delete directory "%s".', \realpath($path)));
            }
        }
    }
}
