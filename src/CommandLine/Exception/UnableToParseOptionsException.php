<?php

declare(strict_types=1);

namespace beram\PiggyStatic\CommandLine\Exception;

final class UnableToParseOptionsException extends \RuntimeException
{
}
