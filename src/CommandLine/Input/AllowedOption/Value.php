<?php

declare(strict_types=1);

namespace beram\PiggyStatic\CommandLine\Input\AllowedOption;

enum Value: string
{
    case none = '';
    case required = ':';
    case optional = '::';
}
