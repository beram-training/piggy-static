<?php

declare(strict_types=1);

namespace beram\PiggyStatic\CommandLine\Input;

use beram\PiggyStatic\CommandLine\Input\AllowedOption\Value;

final class AllowedOptions
{
    public function __construct(
        /** @var AllowedOption[] */
        public readonly array $options,
    ) {
    }

    public function with(string $name, Value $valueType): self
    {
        $options = $this->options;
        $options[$name] = new AllowedOption($name, $valueType);

        return new self($options);
    }
}
