<?php

declare(strict_types=1);

namespace beram\PiggyStatic\CommandLine\Input;

use beram\PiggyStatic\CommandLine\Input\AllowedOption\Value;

final class AllowedOption
{
    public function __construct(
        public readonly string $name,
        public readonly Value $value
    ) {
    }
}
