<?php

declare(strict_types=1);

namespace beram\PiggyStatic\CommandLine\Input;

use beram\PiggyStatic\CommandLine\Exception\UndefinedOptionException;

final class Options
{
    public function __construct(
        /** @var array<string, string|false> */
        private readonly array $options,
    ) {
    }

    public function has(string $name): bool
    {
        return isset($this->options[$name]);
    }

    public function get(string $name): string|false
    {
        return $this->options[$name] ?? throw new UndefinedOptionException('undefined option');
    }
}
