<?php

declare(strict_types=1);

namespace beram\PiggyStatic\CommandLine;

use beram\PiggyStatic\CommandLine\Output\StdStream;

abstract class Command
{
    abstract protected function configure(): Config;

    abstract protected function execute(Input $input, Output $output): ExitCode;

    public function __invoke(): ExitCode
    {
        $config = $this->configure();

        $input = Input::fromGlobals($config->options);
        $output = new StdStream();

        if ($input->options->has('help')) {
            $output->out($this->help($config));

            return ExitCode::OK;
        }

        return $this->execute($input, $output);
    }

    protected function help(Config $config): string
    {
        return \sprintf('Usage: %s [options]', $config->name);
    }
}
