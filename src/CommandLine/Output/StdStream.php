<?php

declare(strict_types=1);

namespace beram\PiggyStatic\CommandLine\Output;

use beram\PiggyStatic\CommandLine\Output;

final class StdStream implements Output
{
    public function out(string $text): void
    {
        $output = \fopen('php://stdout', 'w');
        \fwrite($output, $text.\PHP_EOL);
        \fclose($output);
    }

    public function error(string $text): void
    {
        $output = \fopen('php://stderr', 'w');
        \fwrite($output, $text.\PHP_EOL);
        \fclose($output);
    }
}
