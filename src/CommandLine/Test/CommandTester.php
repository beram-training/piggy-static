<?php

declare(strict_types=1);

namespace beram\PiggyStatic\CommandLine\Test;

use beram\PiggyStatic\CommandLine\Command;
use beram\PiggyStatic\CommandLine\ExitCode;
use beram\PiggyStatic\CommandLine\Input;
use beram\PiggyStatic\CommandLine\Output;

final class CommandTester
{
    /**
     * @psalm-suppress MixedInferredReturnType
     */
    public function execute(Command $command, Input $input, Output $output): ExitCode
    {
        $reflection = new \ReflectionClass($command);
        $execute = $reflection->getMethod('execute');

        /** @psalm-suppress MixedReturnStatement */
        return $execute->invoke($command, $input, $output);
    }
}
