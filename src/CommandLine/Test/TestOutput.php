<?php

declare(strict_types=1);

namespace beram\PiggyStatic\CommandLine\Test;

use beram\PiggyStatic\CommandLine\Output;

final class TestOutput implements Output
{
    /** @var string[] */
    private array $out = [];
    /** @var string[] */
    private array $error = [];

    public function out(string $text): void
    {
        $this->out[] = $text;
    }

    public function error(string $text): void
    {
        $this->error[] = $text;
    }

    public function getOut(): array
    {
        return $this->out;
    }

    public function getError(): array
    {
        return $this->error;
    }
}
