<?php

declare(strict_types=1);

namespace beram\PiggyStatic\CommandLine;

use beram\PiggyStatic\CommandLine\Input\AllowedOption;
use beram\PiggyStatic\CommandLine\Input\AllowedOptions;

final class Config
{
    public function __construct(
        public readonly string $name,
        public readonly AllowedOptions $options,
    ) {
    }

    public static function default(string $name): self
    {
        return new self($name, new AllowedOptions([new AllowedOption('help', AllowedOption\Value::none)]));
    }

    public function withOption(string $name, AllowedOption\Value $valueType): self
    {
        return new self($this->name, $this->options->with($name, $valueType));
    }
}
