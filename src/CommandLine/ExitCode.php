<?php

declare(strict_types=1);

namespace beram\PiggyStatic\CommandLine;

enum ExitCode: int {
    case OK = 0;
    case ERROR = 1;
}
