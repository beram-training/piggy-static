<?php

declare(strict_types=1);

namespace beram\PiggyStatic\CommandLine;

use beram\PiggyStatic\CommandLine\Exception\UnableToParseOptionsException;
use beram\PiggyStatic\CommandLine\Input\AllowedOptions;
use beram\PiggyStatic\CommandLine\Input\Options;

final class Input
{
    public function __construct(
        public readonly Options $options,
    ) {
    }

    public static function fromGlobals(AllowedOptions $allowedOptions): self
    {
        $options = [];
        foreach ($allowedOptions->options as $allowedOption) {
            $options[] = $allowedOption->name.$allowedOption->value->value;
        }
        /** @var array<string, string|false>|false $parsedOptions */
        $parsedOptions = \getopt('', $options);

        if (!\is_array($parsedOptions)) {
            throw new UnableToParseOptionsException('Fail to parse options.');
        }

        return new self(new Options($parsedOptions));
    }
}
