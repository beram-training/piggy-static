<?php

declare(strict_types=1);

namespace beram\PiggyStatic\CommandLine;

interface Output
{
    public function out(string $text): void;

    public function error(string $text): void;
}
