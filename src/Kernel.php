<?php

declare(strict_types=1);

namespace beram\PiggyStatic;

use beram\PiggyStatic\DependencyInjection\Container;
use beram\PiggyStatic\DependencyInjection\Exception\InvalidContainerException;

final class Kernel
{
    public const VERSION = '0.0.1';

    public readonly Container $container;

    public function __construct()
    {
        $container = include __DIR__.'/../config/container.php';
        if (!$container instanceof Container) {
            throw new InvalidContainerException('Check your config!');
        }
        $this->container = $container;
    }
}
