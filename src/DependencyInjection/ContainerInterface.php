<?php

declare(strict_types=1);

namespace beram\PiggyStatic\DependencyInjection;

use beram\PiggyStatic\DependencyInjection\Exception\ContainerExceptionInterface;
use beram\PiggyStatic\DependencyInjection\Exception\NotFoundExceptionInterface;

interface ContainerInterface
{
    /**
     * Finds an entry of the container by its identifier and returns it.
     *
     * @param string $id Identifier of the entry to look for.
     *
     * @throws NotFoundExceptionInterface  No entry was found for **this** identifier.
     * @throws ContainerExceptionInterface Error while retrieving the entry.
     *
     * @return mixed Entry.
     */
    public function get(string $id);

    public function has(string $id): bool;

    /** @return array<string, Definition> */
    public function getDefinitions(): array;
}
