<?php

declare(strict_types=1);

namespace beram\PiggyStatic\DependencyInjection;

use beram\PiggyStatic\DependencyInjection\Exception\CircularDependencyException;
use beram\PiggyStatic\DependencyInjection\Exception\ServiceNotFoundException;

final class Container implements ContainerInterface
{
    /** @var array<string, Definition> */
    private array $definitions = [];
    /** @var array<string, mixed> */
    private array $services = [];
    /** @var array<int, string> */
    private array $dependencyPath = [];

    /**
     * @param array<string, Definition> $definitions
     */
    public function __construct(array $definitions)
    {
        $this->definitions = $definitions;
    }

    /**
     * {@inheritdoc}
     */
    public function get(string $id): mixed
    {
        if (false === isset($this->definitions[$id])) {
            throw new ServiceNotFoundException($id);
        }

        // @todo: detect circular dependencies without global state.
        if (\in_array($id, $this->dependencyPath, true)) {
            $path = $this->dependencyPath;
            $path[] = $id;
            throw new CircularDependencyException(\sprintf('Circular dependency detected: %s.', \implode(' > ', $path)));
        }

        $this->dependencyPath[] = $id;

        try {
            return $this->services[$id] ?? $this->services[$id] = ($this->definitions[$id]->getCallable())($this);
        } finally {
            \array_pop($this->dependencyPath);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function has(string $id): bool
    {
        return isset($this->services[$id]) || isset($this->definitions[$id]);
    }

    /**
     * {@inheritdoc}
     */
    public function getDefinitions(): array
    {
        return $this->definitions;
    }
}
