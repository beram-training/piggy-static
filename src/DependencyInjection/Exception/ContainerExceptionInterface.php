<?php

declare(strict_types=1);

namespace beram\PiggyStatic\DependencyInjection\Exception;

interface ContainerExceptionInterface extends \Throwable
{
}
