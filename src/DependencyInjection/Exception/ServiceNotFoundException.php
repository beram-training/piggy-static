<?php

declare(strict_types=1);

namespace beram\PiggyStatic\DependencyInjection\Exception;

use Throwable;

final class ServiceNotFoundException extends \RuntimeException implements NotFoundExceptionInterface
{
    public function __construct(string $id, int $code = 0, Throwable $previous = null)
    {
        parent::__construct(\sprintf('Service "%s" does not exist.', $id), $code, $previous);
    }
}
