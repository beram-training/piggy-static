<?php

declare(strict_types=1);

namespace beram\PiggyStatic\DependencyInjection\Exception;

final class InvalidContainerException extends \RuntimeException
{
}
