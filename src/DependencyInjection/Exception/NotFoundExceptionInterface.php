<?php

declare(strict_types=1);

namespace beram\PiggyStatic\DependencyInjection\Exception;

/**
 * No entry was found in the container.
 */
interface NotFoundExceptionInterface extends ContainerExceptionInterface
{
}
