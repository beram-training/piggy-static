<?php

declare(strict_types=1);

namespace beram\PiggyStatic\DependencyInjection;

final class Definition
{
    public function __construct(
        private \Closure $callable,
        private int $weight = 0,
    ) {
    }

    public function getCallable(): \Closure
    {
        return $this->callable;
    }

    public function getWeight(): int
    {
        return $this->weight;
    }
}
