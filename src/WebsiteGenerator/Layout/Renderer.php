<?php

declare(strict_types=1);

namespace beram\PiggyStatic\WebsiteGenerator\Layout;

use beram\PiggyStatic\MarkupLanguage\Markdawn\Compiled\Document;
use beram\PiggyStatic\WebsiteGenerator\Exception\ImpossibleLayoutRenderException;
use beram\PiggyStatic\WebsiteGenerator\Layout;
use beram\PiggyStatic\WebsiteGenerator\Site;

final class Renderer
{
    public function render(Layout $layout, Document $document, Site $site): string
    {
        /**
         * @psalm-suppress UnusedVariable
         * @psalm-suppress UnusedClosureParam
         */
        $sandbox = static function (string $path, Document $document, Site $site): string|false {
            \ob_start();
            $title = $document->metadata->hasProperty('title') ? $document->metadata->getProperty('title')->value : null;
            $content = $document->html->value;
            /** @psalm-suppress UnresolvableInclude */
            include $path;

            return \ob_get_clean();
        };

        $result = $sandbox($layout->path, $document, $site);
        if (false === $result) {
            throw new ImpossibleLayoutRenderException(\sprintf('Cannot render document with layout "%s".', $layout->path));
        }

        return $result;
    }
}
