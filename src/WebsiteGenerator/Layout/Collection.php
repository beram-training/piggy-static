<?php

declare(strict_types=1);

namespace beram\PiggyStatic\WebsiteGenerator\Layout;

use beram\PiggyStatic\WebsiteGenerator\Exception\UndefinedLayoutException;
use beram\PiggyStatic\WebsiteGenerator\Layout;

final class Collection
{
    public function __construct(
        /** @var array<string, Layout> */
        private readonly array $layouts,
    ) {
    }

    /**
     * @return array<string, Layout>
     */
    public function all(): array
    {
        return $this->layouts;
    }

    public function has(string $id): bool
    {
        return isset($this->layouts[$id]);
    }

    public function get(string $id): Layout
    {
        return $this->layouts[$id] ?? throw new UndefinedLayoutException(\sprintf('Undefined layout "%s".', $id));
    }
}
