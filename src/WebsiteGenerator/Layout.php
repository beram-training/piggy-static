<?php

declare(strict_types=1);

namespace beram\PiggyStatic\WebsiteGenerator;

use beram\PiggyStatic\WebsiteGenerator\Exception\InvalidLayoutException;

final class Layout
{
    public function __construct(
        public readonly string $path,
    ) {
        if (false === \is_file($this->path)) {
            throw new InvalidLayoutException(\sprintf('Invalid layout path "%s".', $this->path));
        }
    }
}
