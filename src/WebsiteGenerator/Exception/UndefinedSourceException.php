<?php

declare(strict_types=1);

namespace beram\PiggyStatic\WebsiteGenerator\Exception;

final class UndefinedSourceException extends \RuntimeException
{
}
