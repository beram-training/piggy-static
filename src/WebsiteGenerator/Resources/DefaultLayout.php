<?php

declare(strict_types=1);

namespace beram\PiggyStatic\WebsiteGenerator\Resources;

use beram\PiggyStatic\WebsiteGenerator\Layout;

final class DefaultLayout
{
    public readonly Layout $layout;

    public function __construct()
    {
        $this->layout = new Layout(__DIR__.'/_layouts/default.html');
    }
}
