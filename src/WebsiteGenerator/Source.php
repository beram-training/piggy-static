<?php

declare(strict_types=1);

namespace beram\PiggyStatic\WebsiteGenerator;

use beram\PiggyStatic\MarkupLanguage\Markdawn\Compiled\Document;

final class Source
{
    public function __construct(
        public readonly string $relativePathToSrc,
        public readonly \SplFileInfo $file,
        public readonly Document $document,
        public readonly string $relativeUrl,
    ) {
    }
}
