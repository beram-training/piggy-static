<?php

declare(strict_types=1);

namespace beram\PiggyStatic\WebsiteGenerator\Command;

use beram\PiggyStatic\CommandLine\Command;
use beram\PiggyStatic\CommandLine\Config as CliConfig;
use beram\PiggyStatic\CommandLine\ExitCode;
use beram\PiggyStatic\CommandLine\Input;
use beram\PiggyStatic\CommandLine\Input\AllowedOption;
use beram\PiggyStatic\CommandLine\Output;
use beram\PiggyStatic\FileSystem\Exception\FileSystemException;
use beram\PiggyStatic\FileSystem\FileSystem;
use beram\PiggyStatic\Kernel;
use beram\PiggyStatic\MarkupLanguage\Markdawn\Compiled\Document;
use beram\PiggyStatic\WebsiteGenerator\Config;
use beram\PiggyStatic\WebsiteGenerator\Layout;
use beram\PiggyStatic\WebsiteGenerator\Resources\DefaultLayout;
use beram\PiggyStatic\WebsiteGenerator\Site;
use beram\PiggyStatic\WebsiteGenerator\Source;
use beram\PiggyStatic\WebsiteGenerator\Source\SourceLocator;

final class Generate extends Command
{
    public function __construct(
        private readonly SourceLocator $sourceLocator,
        private readonly Layout\Renderer $layoutRenderer,
    ) {
    }

    protected function configure(): CliConfig
    {
        return CliConfig::default('generate')
            ->withOption('config', AllowedOption\Value::required)
            ;
    }

    protected function help(CliConfig $config): string
    {
        $help = <<<'CLI'
Piggy Static Website Generator
Version: %s
Usage: %s [options]

Options:
    --config    the path to the config file. Default to ".piggy-static.php".
CLI;

        return \sprintf($help, Kernel::VERSION, $config->name);
    }

    protected function execute(Input $input, Output $output): ExitCode
    {
        $configFile = $input->options->has('config') ? $input->options->get('config') : '.piggy-static.php';

        if (false === $configFile) {
            $output->error('Cannot find config file.');

            return ExitCode::ERROR;
        }
        if (false === \is_file($configFile)) {
            $output->error(\sprintf('Cannot find config file "%s".', $configFile));

            return ExitCode::ERROR;
        }
        $config = include $configFile;
        if (!$config instanceof Config) {
            $output->error(\sprintf('Invalid config file "%s".', $configFile));

            return ExitCode::ERROR;
        }

        if (false === $this->prepareDestination($config->dest)) {
            $output->error(\sprintf('Cannot prepare destination directory "%s".', $config->dest));

            return ExitCode::ERROR;
        }
        $destAbsolute = \realpath($config->dest);

        $sources = $this->sourceLocator->locate($config);
        $site = new Site($sources);

        /** @var Source $source */
        foreach ($sources as $source) {
            $destpath = \sprintf('%s/%s', $destAbsolute, $source->relativeUrl);
            @\mkdir(\dirname($destpath), 0755, true);

            $layout = $this->getLayoutForDocument($source->document, $config);

            if (false === \file_put_contents($destpath, $this->layoutRenderer->render($layout, $source->document, $site))) {
                $output->error(\sprintf('Cannot write file "%s".', $destpath));

                return ExitCode::ERROR;
            }
        }

        $assetsDirectory = \sprintf('%s/assets', \realpath($config->src));
        if (\is_dir($assetsDirectory)) {
            FileSystem::copy($assetsDirectory, \sprintf('%s/assets', \realpath($config->dest)));
        }

        return ExitCode::OK;
    }

    private function prepareDestination(string $dest): bool
    {
        try {
            FileSystem::delete($dest);
            FileSystem::createDir($dest, 0755);
        } catch (FileSystemException $exception) {
            return false;
        }

        return true;
    }

    private function getLayoutForDocument(Document $document, Config $config): Layout
    {
        $defaultLayout = $config->layouts->has('default') ? $config->layouts->get('default') : (new DefaultLayout())->layout;
        if (false === $document->metadata->hasProperty('layout')) {
            return $defaultLayout;
        }

        $id = $document->metadata->getProperty('layout')->value;
        if (false === $config->layouts->has($id)) {
            return $defaultLayout;
        }

        return $config->layouts->get($id);
    }
}
