<?php

declare(strict_types=1);

namespace beram\PiggyStatic\WebsiteGenerator;

use beram\PiggyStatic\WebsiteGenerator\Layout\Collection;

final class Config
{
    public function __construct(
        public readonly string $src,
        public readonly string $dest,
        public readonly Collection $layouts,
        public readonly array $filesToExclude,
    ) {
    }

    public static function default(): self
    {
        return new self(
            'src',
            'build',
            new Collection([]),
            ['.git', '.idea', '.DS_Store', 'assets'],
        );
    }

    public function withSrc(string $src): self
    {
        return new self(
            $src,
            $this->dest,
            $this->layouts,
            $this->filesToExclude,
        );
    }

    public function withDest(string $dest): self
    {
        return new self(
            $this->src,
            $dest,
            $this->layouts,
            $this->filesToExclude,
        );
    }

    public function withLayouts(Collection $layouts): self
    {
        return new self(
            $this->src,
            $this->dest,
            $layouts,
            $this->filesToExclude,
        );
    }

    public function withLayout(string $id, Layout $layout): self
    {
        $layouts = $this->layouts->all();
        $layouts[$id] = $layout;

        return $this->withLayouts(new Collection($layouts));
    }

    public function withFilesToExclude(array $files): self
    {
        return new self(
            $this->src,
            $this->dest,
            $this->layouts,
            \array_merge($this->filesToExclude, $files),
        );
    }
}
