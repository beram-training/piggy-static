<?php

declare(strict_types=1);

namespace beram\PiggyStatic\WebsiteGenerator;

final class Site
{
    public function __construct(
        public readonly Source\Collection $sources,
    ) {
    }
}
