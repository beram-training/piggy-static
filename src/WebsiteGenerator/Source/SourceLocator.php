<?php

declare(strict_types=1);

namespace beram\PiggyStatic\WebsiteGenerator\Source;

use beram\PiggyStatic\FileSystem\Finder;
use beram\PiggyStatic\MarkupLanguage\Markdawn\Interpreter;
use beram\PiggyStatic\WebsiteGenerator\Config;
use beram\PiggyStatic\WebsiteGenerator\Source;

final class SourceLocator
{
    public function __construct(
        private readonly Interpreter $interpreter,
    ) {
    }

    public function locate(Config $config): Collection
    {
        $sourcesToProcess = Finder::create()
            ->in($config->src)
            ->exclude($config->filesToExclude)
            ->find();

        $srcInfo = new \SplFileInfo($config->src);

        $sources = [];

        /** @var \SplFileInfo $source */
        foreach ($sourcesToProcess as $source) {
            if ('md' !== $source->getExtension()) {
                continue;
            }

            $document = $this->interpreter->fileToDocument($source->getRealPath());
            $relativePathToSrc = \str_replace($srcInfo->getPathname(), '', $source->getPath());
            $relativePathToSrcWithFilename = \sprintf('%s/%s', $relativePathToSrc, $source->getBasename());
            $sources[$relativePathToSrcWithFilename] = new Source($relativePathToSrcWithFilename, $source, $document, \sprintf('%s/%s.html', $relativePathToSrc, $source->getBasename('.md')));
        }

        \ksort($sources);

        return new Collection($sources);
    }
}
