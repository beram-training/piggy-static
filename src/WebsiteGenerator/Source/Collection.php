<?php

declare(strict_types=1);

namespace beram\PiggyStatic\WebsiteGenerator\Source;

use beram\PiggyStatic\WebsiteGenerator\Exception\UndefinedSourceException;
use beram\PiggyStatic\WebsiteGenerator\Source;

final class Collection implements \IteratorAggregate
{
    public function __construct(
        /** @var array<string, Source> */
        private readonly array $sources,
    ) {
    }

    /**
     * @return array<string, Source>
     */
    public function all(): array
    {
        return $this->sources;
    }

    public function has(string $id): bool
    {
        return isset($this->sources[$id]);
    }

    public function get(string $id): Source
    {
        return $this->sources[$id] ?? throw new UndefinedSourceException(\sprintf('Undefined source "%s".', $id));
    }

    public function getIterator(): \ArrayIterator
    {
        return new \ArrayIterator($this->sources);
    }
}
