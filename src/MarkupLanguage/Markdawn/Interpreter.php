<?php

declare(strict_types=1);

namespace beram\PiggyStatic\MarkupLanguage\Markdawn;

final class Interpreter
{
    public function __construct(
        public readonly Lexer $lexer,
        public readonly Parser $parser,
        public readonly Compiler $compiler,
    ) {
    }

    public function fileToDocument(string $filepath): Compiled\Document
    {
        return $this->compiler->compile($this->parser->parse($this->lexer->tokenize(Source::fromFilePath($filepath))));
    }
}
