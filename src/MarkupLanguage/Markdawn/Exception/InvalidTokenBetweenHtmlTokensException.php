<?php

declare(strict_types=1);

namespace beram\PiggyStatic\MarkupLanguage\Markdawn\Exception;

final class InvalidTokenBetweenHtmlTokensException extends \DomainException
{
}
