<?php

declare(strict_types=1);

namespace beram\PiggyStatic\MarkupLanguage\Markdawn\Exception;

final class UndefinedPropertyException extends \DomainException
{
    public function __construct(string $name)
    {
        parent::__construct(\sprintf('Unknown property "%s".', $name));
    }
}
