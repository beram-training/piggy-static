<?php

declare(strict_types=1);

namespace beram\PiggyStatic\MarkupLanguage\Markdawn\Exception;

final class InvalidParsedCodeAttributeException extends \DomainException
{
}
