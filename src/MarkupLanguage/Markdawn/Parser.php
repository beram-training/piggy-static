<?php

declare(strict_types=1);

namespace beram\PiggyStatic\MarkupLanguage\Markdawn;

use beram\PiggyStatic\MarkupLanguage\Markdawn\Exception\InvalidParsedCodeAttributeException;
use beram\PiggyStatic\MarkupLanguage\Markdawn\Exception\InvalidParsedImageAttributeException;
use beram\PiggyStatic\MarkupLanguage\Markdawn\Exception\InvalidParsedLinkAttributeException;
use beram\PiggyStatic\MarkupLanguage\Markdawn\Exception\InvalidTokenBetweenCodeTokensException;
use beram\PiggyStatic\MarkupLanguage\Markdawn\Exception\InvalidTokenBetweenCommentTokensException;
use beram\PiggyStatic\MarkupLanguage\Markdawn\Exception\InvalidTokenBetweenHtmlTokensException;
use beram\PiggyStatic\MarkupLanguage\Markdawn\Exception\InvalidTokenBetweenMetadataTokensException;
use beram\PiggyStatic\MarkupLanguage\Markdawn\Node\Document;
use beram\PiggyStatic\MarkupLanguage\Markdawn\Node\Kind\Html\Blockquote;
use beram\PiggyStatic\MarkupLanguage\Markdawn\Node\Kind\Html\Code;
use beram\PiggyStatic\MarkupLanguage\Markdawn\Node\Kind\Html\Comment;
use beram\PiggyStatic\MarkupLanguage\Markdawn\Node\Kind\Html\Emphasis;
use beram\PiggyStatic\MarkupLanguage\Markdawn\Node\Kind\Html\Heading1;
use beram\PiggyStatic\MarkupLanguage\Markdawn\Node\Kind\Html\Heading2;
use beram\PiggyStatic\MarkupLanguage\Markdawn\Node\Kind\Html\Heading3;
use beram\PiggyStatic\MarkupLanguage\Markdawn\Node\Kind\Html\Heading4;
use beram\PiggyStatic\MarkupLanguage\Markdawn\Node\Kind\Html\Heading5;
use beram\PiggyStatic\MarkupLanguage\Markdawn\Node\Kind\Html\Heading6;
use beram\PiggyStatic\MarkupLanguage\Markdawn\Node\Kind\Html\HorizontalRule;
use beram\PiggyStatic\MarkupLanguage\Markdawn\Node\Kind\Html\Image;
use beram\PiggyStatic\MarkupLanguage\Markdawn\Node\Kind\Html\Link;
use beram\PiggyStatic\MarkupLanguage\Markdawn\Node\Kind\Html\Paragraph;
use beram\PiggyStatic\MarkupLanguage\Markdawn\Node\Kind\Html\Strikethrough;
use beram\PiggyStatic\MarkupLanguage\Markdawn\Node\Kind\Html\Strong;
use beram\PiggyStatic\MarkupLanguage\Markdawn\Node\Kind\Html\Text;
use beram\PiggyStatic\MarkupLanguage\Markdawn\Node\Kind\Metadata\Property;

final class Parser
{
    private const ALLOWED_TOKEN_TYPES_FOR_PARAGRAPH = [
        TokenType::TEXT,
        TokenType::STRONG,
        TokenType::EMPHASIS,
        TokenType::STRIKETHROUGH,
        TokenType::LINK,
        TokenType::IMAGE,
    ];

    /**
     * @param Token[] $tokens
     */
    public function parse(array $tokens): Document
    {
        $stream = new TokenStream($tokens);
        $nodes = [];
        $metadata = [];

        while (false === $stream->isEOF()) {
            switch ($stream->getCurrent()->type) {
                case TokenType::METADATA_BEGIN:
                    $metadata = $this->parseMetadata($stream);
                    $stream->next();
                    break;
                case TokenType::HEADING1:
                    $nodes[] = $this->parseHeading1($stream);
                    $stream->next();
                    break;
                case TokenType::HEADING2:
                    $nodes[] = $this->parseHeading2($stream);
                    $stream->next();
                    break;
                case TokenType::HEADING3:
                    $nodes[] = $this->parseHeading3($stream);
                    $stream->next();
                    break;
                case TokenType::HEADING4:
                    $nodes[] = $this->parseHeading4($stream);
                    $stream->next();
                    break;
                case TokenType::HEADING5:
                    $nodes[] = $this->parseHeading5($stream);
                    $stream->next();
                    break;
                case TokenType::HEADING6:
                    $nodes[] = $this->parseHeading6($stream);
                    $stream->next();
                    break;
                case TokenType::IMAGE:
                    $nodes[] = $this->parseImage($stream);
                    $stream->next();
                    break;
                case TokenType::LINK:
                    $nodes[] = $this->parseLink($stream);
                    $stream->next();
                    break;
                case TokenType::HORIZONTAL_RULE:
                    $nodes[] = $this->parseHorizontalRule();
                    $stream->next();
                    break;
                case TokenType::CODE_BEGIN:
                    $nodes[] = $this->parseCode($stream);
                    $stream->next();
                    break;
                case TokenType::BLOCKQUOTE_BEGIN:
                    $nodes[] = $this->parseBlockquote($stream);
                    $stream->next();
                    break;
                case TokenType::COMMENT_BEGIN:
                    $stream->next();
                    $nodes[] = $this->parseComment($stream);
                    $stream->next();
                    break;
                case TokenType::HTML_BEGIN:
                    $stream->next();
                    $nodes[] = $this->parseHtml($stream);
                    $stream->next();
                    break;
                case TokenType::LINE_BLANK:
                    // Paragraph.
                    if (\in_array($stream->lookAt(1)->type, self::ALLOWED_TOKEN_TYPES_FOR_PARAGRAPH, true)) {
                        $stream->next();
                        $nodes[] = $this->parseParagraph($stream);
                        $stream->next();
                        break;
                    }
                    $stream->next();
                    break;
                default:
                    // Paragraph.
                    if (\in_array($stream->getCurrent()->type, self::ALLOWED_TOKEN_TYPES_FOR_PARAGRAPH, true)) {
                        $nodes[] = $this->parseParagraph($stream);
                        $stream->next();
                        break;
                    }
                    $stream->next();
            }
        }

        return new Document($nodes, $metadata);
    }

    /**
     * @return Node\Kind\Metadata[]
     */
    private function parseMetadata(TokenStream $stream): array
    {
        $metadata = [];
        $tokens = $stream->nextUntil(TokenType::METADATA_END);
        $substream = new TokenStream($tokens);
        do {
            switch ($substream->getCurrent()->type) {
                case TokenType::METADATA_PROPERTY:
                    $propertyName = new Property\Name($substream->getCurrent()->value);
                    $substream->next();
                    if (TokenType::METADATA_OPERATOR_ASSIGNMENT !== $substream->getCurrent()->type) {
                        throw new InvalidTokenBetweenMetadataTokensException(\sprintf('Expected "%s", got "%s"', TokenType::METADATA_OPERATOR_ASSIGNMENT->name, $substream->getCurrent()->type->name));
                    }
                    $substream->next();
                    if (TokenType::METADATA_VALUE !== $substream->getCurrent()->type) {
                        throw new InvalidTokenBetweenMetadataTokensException(\sprintf('Expected "%s", got "%s"', TokenType::METADATA_VALUE->name, $substream->getCurrent()->type->name));
                    }
                    $propertyValue = new Property\Value($substream->getCurrent()->value);
                    $metadata[] = new Property($propertyName, $propertyValue);
                    $substream->next();
                    break;
                case TokenType::METADATA_BEGIN:
                case TokenType::METADATA_END:
                case TokenType::EOL:
                    $substream->tryNext();
                    break;
                default:
                    throw new InvalidTokenBetweenMetadataTokensException(\sprintf('Invalid token type: "%s"', $substream->getCurrent()->type->name));
            }
        } while ($substream->isAbleToContinueLoop());

        return $metadata;
    }

    private function parseText(TokenStream $stream): Text
    {
        return new Text($stream->getCurrent()->value);
    }

    /**
     * @return Node\Kind\Html[]
     */
    private function parseHeadingX(TokenStream $stream): array
    {
        $tokens = $stream->nextUntil(TokenType::EOL);
        $nodes = [];
        foreach ($tokens as $token) {
            switch ($token->type) {
                case TokenType::TEXT:
                    $nodes[] = new Text($token->value);
                    break;
            }
        }

        return $nodes;
    }

    private function parseHeading1(TokenStream $stream): Heading1
    {
        return new Heading1($this->parseHeadingX($stream));
    }

    private function parseHeading2(TokenStream $stream): Heading2
    {
        return new Heading2($this->parseHeadingX($stream));
    }

    private function parseHeading3(TokenStream $stream): Heading3
    {
        return new Heading3($this->parseHeadingX($stream));
    }

    private function parseHeading4(TokenStream $stream): Heading4
    {
        return new Heading4($this->parseHeadingX($stream));
    }

    private function parseHeading5(TokenStream $stream): Heading5
    {
        return new Heading5($this->parseHeadingX($stream));
    }

    private function parseHeading6(TokenStream $stream): Heading6
    {
        return new Heading6($this->parseHeadingX($stream));
    }

    private function parseStrong(TokenStream $stream): Strong
    {
        $tokens = $stream->nextUntil(TokenType::STRONG);
        $substream = new TokenStream($tokens);
        $nodes = [];
        do {
            switch ($substream->getCurrent()->type) {
                case TokenType::TEXT:
                    $nodes[] = $this->parseText($substream);
                    $substream->tryNext();
                    break;
                case TokenType::EMPHASIS:
                    $substream->tryNext();
                    $nodes[] = $this->parseEmphasis($substream);
                    $substream->tryNext();
                    break;
                case TokenType::STRIKETHROUGH:
                    $substream->tryNext();
                    $nodes[] = $this->parseStrikethrough($substream);
                    $substream->tryNext();
                    break;
                default:
                    $substream->tryNext();
                    break;
            }
        } while ($substream->isAbleToContinueLoop());

        return new Strong($nodes);
    }

    private function parseEmphasis(TokenStream $stream): Emphasis
    {
        $tokens = $stream->nextUntil(TokenType::EMPHASIS);
        $substream = new TokenStream($tokens);
        $nodes = [];
        do {
            switch ($substream->getCurrent()->type) {
                case TokenType::TEXT:
                    $nodes[] = $this->parseText($substream);
                    $substream->tryNext();
                    break;
                case TokenType::STRONG:
                    $substream->tryNext();
                    $nodes[] = $this->parseStrong($substream);
                    $substream->tryNext();
                    break;
                case TokenType::STRIKETHROUGH:
                    $substream->tryNext();
                    $nodes[] = $this->parseStrikethrough($substream);
                    $substream->tryNext();
                    break;
                default:
                    $substream->tryNext();
                    break;
            }
        } while ($substream->isAbleToContinueLoop());

        return new Emphasis($nodes);
    }

    private function parseStrikethrough(TokenStream $stream): Strikethrough
    {
        $tokens = $stream->nextUntil(TokenType::STRIKETHROUGH);
        $substream = new TokenStream($tokens);
        $nodes = [];
        do {
            switch ($substream->getCurrent()->type) {
                case TokenType::TEXT:
                    $nodes[] = $this->parseText($substream);
                    $substream->tryNext();
                    break;
                case TokenType::STRONG:
                    $substream->tryNext();
                    $nodes[] = $this->parseStrong($substream);
                    $substream->tryNext();
                    break;
                case TokenType::EMPHASIS:
                    $substream->tryNext();
                    $nodes[] = $this->parseEmphasis($substream);
                    $substream->tryNext();
                    break;
                default:
                    $substream->tryNext();
                    break;
            }
        } while ($substream->isAbleToContinueLoop());

        return new Strikethrough($nodes);
    }

    private function parseImage(TokenStream $stream): Image
    {
        $matches = [];
        \preg_match('#^\!\[(?<alt>.*)\]\((?<src>.*)\)$#', $stream->getCurrent()->value, $matches);

        return new Image(
            new Image\Src($matches['src'] ?? throw new InvalidParsedImageAttributeException('"src" is mandatory')),
            new Image\Alt($matches['alt'] ?? throw new InvalidParsedImageAttributeException('"alt" is mandatory')),
        );
    }

    private function parseLink(TokenStream $stream): Link
    {
        $matches = [];
        \preg_match('#^\[(?<text>.*)\]\((?<href>[^ ]*)( "(?<title>.*)")?\)$#', $stream->getCurrent()->value, $matches);

        return new Link(
            new Link\Text([new Text($matches['text'] ?? throw new InvalidParsedLinkAttributeException('no text is not possible'))]),
            new Link\Href($matches['href'] ?? throw new InvalidParsedLinkAttributeException('no href is not possible')),
            isset($matches['title']) ? new Link\Title($matches['title']) : null,
        );
    }

    private function parseHorizontalRule(): HorizontalRule
    {
        return new HorizontalRule();
    }

    private function parseCode(TokenStream $stream): Code
    {
        $tokens = $stream->nextUntil(TokenType::CODE_END);
        $language = null;
        $text = null;
        foreach ($tokens as $token) {
            switch ($token->type) {
                case TokenType::CODE_BEGIN:
                    $matches = [];
                    \preg_match('#^```(?<language>.*)$#', \preg_quote($token->value, '#'), $matches);
                    $language = Code\Language::from($matches['language'] ?? '');
                    break;
                case TokenType::TEXT:
                    $text = new Code\Text($token->value);
                    break;
                case TokenType::EOL:
                    break;
                default:
                    throw new InvalidTokenBetweenCodeTokensException(\sprintf('Token "%s" is not expected between tokens "%s" and "%s"', $token->type->name, TokenType::CODE_BEGIN->name, TokenType::CODE_END->name));
            }
        }

        if (null === $language) {
            throw new InvalidParsedCodeAttributeException('Missing language.');
        }

        if (null === $text) {
            throw new InvalidParsedCodeAttributeException('Missing text.');
        }

        return new Code($language, $text);
    }

    private function parseParagraph(TokenStream $stream): Paragraph
    {
        $subtokens = $stream->tryNextUntil(TokenType::LINE_BLANK);
        if ([] === $subtokens) {
            $subtokens = $stream->tryNextUntil(TokenType::EOL);
        }
        if ([] === $subtokens) {
            $subtokens = $stream->tokens;
        }

        $substream = new TokenStream($subtokens);
        $nodes = [];
        do {
            switch ($substream->getCurrent()->type) {
                case TokenType::TEXT:
                    $nodes[] = $this->parseText($substream);
                    $substream->tryNext();
                    break;
                case TokenType::STRONG:
                    $substream->tryNext();
                    $nodes[] = $this->parseStrong($substream);
                    $substream->tryNext();
                    break;
                case TokenType::EMPHASIS:
                    $substream->tryNext();
                    $nodes[] = $this->parseEmphasis($substream);
                    $substream->tryNext();
                    break;
                case TokenType::STRIKETHROUGH:
                    $substream->tryNext();
                    $nodes[] = $this->parseStrikethrough($substream);
                    $substream->tryNext();
                    break;
                case TokenType::IMAGE:
                    $nodes[] = $this->parseImage($substream);
                    $substream->tryNext();
                    break;
                case TokenType::LINK:
                    $nodes[] = $this->parseLink($substream);
                    $substream->tryNext();
                    break;
                case TokenType::EOL:
                    if ($substream->hasNext()) {
                        $nodes[] = new Text(' ');
                    }
                    $substream->tryNext();
                    break;
                default:
                    $substream->tryNext();
                    break;
            }
        } while ($substream->isAbleToContinueLoop());

        return new Paragraph($nodes);
    }

    private function parseBlockquote(TokenStream $stream): Blockquote
    {
        $tokens = $stream->nextUntil(TokenType::BLOCKQUOTE_END);
        $substream = new TokenStream($tokens);
        $nodes = [];
        do {
            switch ($substream->getCurrent()->type) {
                case TokenType::BLOCKQUOTE_BEGIN:
                    $substream->tryNext();
                    break;
                case TokenType::HORIZONTAL_RULE:
                    $nodes[] = $this->parseHorizontalRule();
                    $substream->tryNext();
                    break;
                case TokenType::CODE_BEGIN:
                    $nodes[] = $this->parseCode($substream);
                    $substream->tryNext();
                    break;
                default:
                    $nodes[] = $this->parseParagraph($substream);
                    $substream->tryNext();
                    break;
            }
        } while ($substream->isAbleToContinueLoop());

        return new Blockquote($nodes);
    }

    private function parseComment(TokenStream $stream): Comment
    {
        $tokens = $stream->nextUntil(TokenType::COMMENT_END);
        if (1 < \count($tokens)) {
            throw new InvalidTokenBetweenCommentTokensException('Only one text token is allowed.');
        }

        if ([] === $tokens) {
            // @todo: Empty comment is useless and should be skipped.
            return new Comment('');
        }

        $token = \reset($tokens);

        if (TokenType::TEXT !== $token->type) {
            throw new InvalidTokenBetweenCommentTokensException(\sprintf('Only one text token is allowed: "%s" given.', $token->type->name));
        }

        return new Comment($token->value);
    }

    private function parseHtml(TokenStream $stream): Node\Kind\Html\Html
    {
        $tokens = $stream->nextUntil(TokenType::HTML_END);
        if (1 < \count($tokens)) {
            throw new InvalidTokenBetweenHtmlTokensException('Only one text token is allowed.');
        }

        if ([] === $tokens) {
            throw new InvalidTokenBetweenHtmlTokensException('Not text is not allowed.');
        }

        $token = \reset($tokens);

        if (TokenType::TEXT !== $token->type) {
            throw new InvalidTokenBetweenHtmlTokensException(\sprintf('Only one text token is allowed: "%s" given.', $token->type->name));
        }

        return new Node\Kind\Html\Html($token->value);
    }
}
