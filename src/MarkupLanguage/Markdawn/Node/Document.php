<?php

declare(strict_types=1);

namespace beram\PiggyStatic\MarkupLanguage\Markdawn\Node;

use beram\PiggyStatic\MarkupLanguage\Markdawn\Compiled;
use beram\PiggyStatic\MarkupLanguage\Markdawn\Node;

final class Document implements Node
{
    public function __construct(
        /** @var Kind\Html[] */
        public readonly array $nodes,
        /** @var Kind\Metadata[] */
        public readonly array $metadata,
    ) {
    }

    public function compile(): Compiled\Document
    {
        $html = new Compiled\Html('');

        foreach ($this->nodes as $node) {
            $html = $html->concatenateAfter($node->compile());
        }

        $properties = [];
        foreach ($this->metadata as $property) {
            if (!$property instanceof Kind\Metadata\Property) {
                continue;
            }
            $properties[$property->name->value] = new Compiled\Metadata\Property($property->name->value, $property->value->value);
        }

        return new Compiled\Document($html, new Compiled\Metadata($properties));
    }
}
