<?php

declare(strict_types=1);

namespace beram\PiggyStatic\MarkupLanguage\Markdawn\Node\Kind\Metadata\Property;

use beram\PiggyStatic\MarkupLanguage\Markdawn\Node\Kind\Metadata;

final class Value implements Metadata
{
    public function __construct(
        public readonly string $value,
    ) {
    }
}
