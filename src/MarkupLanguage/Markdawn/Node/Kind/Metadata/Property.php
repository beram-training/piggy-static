<?php

declare(strict_types=1);

namespace beram\PiggyStatic\MarkupLanguage\Markdawn\Node\Kind\Metadata;

use beram\PiggyStatic\MarkupLanguage\Markdawn\Node\Kind\Metadata;

final class Property implements Metadata
{
    public function __construct(
        public readonly Property\Name $name,
        public readonly Property\Value $value,
    ) {
    }
}
