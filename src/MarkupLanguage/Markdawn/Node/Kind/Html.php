<?php

declare(strict_types=1);

namespace beram\PiggyStatic\MarkupLanguage\Markdawn\Node\Kind;

use beram\PiggyStatic\MarkupLanguage\Markdawn\Compiled;
use beram\PiggyStatic\MarkupLanguage\Markdawn\Node;

interface Html extends Node
{
    public function compile(): Compiled\Html;
}
