<?php

declare(strict_types=1);

namespace beram\PiggyStatic\MarkupLanguage\Markdawn\Node\Kind\Html;

use beram\PiggyStatic\MarkupLanguage\Markdawn\Compiled\Html;
use beram\PiggyStatic\MarkupLanguage\Markdawn\Node\Kind\Html as HtmlKind;

final class Comment implements HtmlKind
{
    public function __construct(
        public readonly string $value,
    ) {
    }

    public function compile(): Html
    {
        return new Html(\sprintf('<!--%s-->%s', $this->value, \PHP_EOL));
    }
}
