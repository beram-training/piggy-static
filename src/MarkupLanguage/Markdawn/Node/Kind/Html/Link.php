<?php

declare(strict_types=1);

namespace beram\PiggyStatic\MarkupLanguage\Markdawn\Node\Kind\Html;

use beram\PiggyStatic\MarkupLanguage\Markdawn\Compiled\Html;
use beram\PiggyStatic\MarkupLanguage\Markdawn\Node\Kind\Html as HtmlKind;

final class Link implements HtmlKind
{
    public function __construct(
        public readonly Link\Text $text,
        public readonly Link\Href $href,
        public readonly ?Link\Title $title,
    ) {
    }

    public function compile(): Html
    {
        return new Html(\sprintf(
            '<a%s%s>%s</a>',
            (string) $this->href->compile(),
            (string) $this->title?->compile(),
            (string) $this->text->compile(),
        ));
    }
}
