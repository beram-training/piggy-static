<?php

declare(strict_types=1);

namespace beram\PiggyStatic\MarkupLanguage\Markdawn\Node\Kind\Html\Link;

use beram\PiggyStatic\MarkupLanguage\Markdawn\Compiled\Html;
use beram\PiggyStatic\MarkupLanguage\Markdawn\Node\Kind\Html as HtmlKind;

final class Href implements HtmlKind
{
    public function __construct(
        public readonly string $value
    ) {
    }

    public function compile(): Html
    {
        return new Html(\sprintf(' href="%s"', $this->value));
    }
}
