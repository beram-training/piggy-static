<?php

declare(strict_types=1);

namespace beram\PiggyStatic\MarkupLanguage\Markdawn\Node\Kind\Html\Code;

enum Language: string
{
    case none = '';
    case php = 'php';
    case awk = 'awk';
    case bash = 'bash';
    case diff = 'diff';
    case dlang = 'd';
    case dockerfile = 'dockerfile';
    case gherkin = 'gherkin';
    case go = 'go';
    case http = 'http';
    case html = 'html';
    case ini = 'ini';
    case json = 'json';
    case makefile = 'makefile';
    case markdown = 'markdown';
    case nginx = 'nginx';
    case perl = 'perl';
    case plaintext = 'plaintext';
    case rust = 'rust';
    case shell = 'shell';
    case sql = 'sql';
    case twig = 'twig';
    case xml = 'xml';
    case yaml = 'yaml';
}
