<?php

declare(strict_types=1);

namespace beram\PiggyStatic\MarkupLanguage\Markdawn\Node\Kind\Html;

use beram\PiggyStatic\MarkupLanguage\Markdawn\Compiled\Html;
use beram\PiggyStatic\MarkupLanguage\Markdawn\Node\Kind\Html as HtmlKind;

final class Code implements HtmlKind
{
    public function __construct(
        public readonly Code\Language $language,
        public readonly Code\Text $text,
    ) {
    }

    public function compile(): Html
    {
        return new Html(\sprintf(
            '<pre><code class="language-%s">%s</code></pre>%s',
            $this->language->value,
            (string) $this->text->compile(),
            \PHP_EOL,
        ));
    }
}
