<?php

declare(strict_types=1);

namespace beram\PiggyStatic\MarkupLanguage\Markdawn\Node\Kind\Html\List_;

use beram\PiggyStatic\MarkupLanguage\Markdawn\Compiled\Html;
use beram\PiggyStatic\MarkupLanguage\Markdawn\Node\Kind\Html as HtmlKind;

final class Unordered implements HtmlKind
{
    public function __construct(
        /** @var \beram\PiggyStatic\MarkupLanguage\Markdawn\Node\Kind\Html\List_\Unordered\Item[] */
        public readonly array $nodes,
    ) {
    }

    public function compile(): Html
    {
        $html = new Html('');

        foreach ($this->nodes as $node) {
            $html = $html->concatenateAfter($node->compile());
        }

        return new Html(\sprintf('<ul>%s</ul>%s', $html->value, \PHP_EOL));
    }
}
