<?php

declare(strict_types=1);

namespace beram\PiggyStatic\MarkupLanguage\Markdawn\Node\Kind\Html;

use beram\PiggyStatic\MarkupLanguage\Markdawn\Compiled\Html;
use beram\PiggyStatic\MarkupLanguage\Markdawn\Node\Kind\Html as HtmlKind;

final class Heading4 implements HtmlKind
{
    public function __construct(
        /** @var HtmlKind[] */
        public readonly array $nodes,
    ) {
    }

    public function compile(): Html
    {
        $html = new Html('');

        foreach ($this->nodes as $node) {
            $html = $html->concatenateAfter($node->compile());
        }

        return new Html(\sprintf('<h4>%s</h4>%s', $html->value, \PHP_EOL));
    }
}
