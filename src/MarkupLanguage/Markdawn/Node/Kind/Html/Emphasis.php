<?php

declare(strict_types=1);

namespace beram\PiggyStatic\MarkupLanguage\Markdawn\Node\Kind\Html;

use beram\PiggyStatic\MarkupLanguage\Markdawn\Compiled\Html;
use beram\PiggyStatic\MarkupLanguage\Markdawn\Node\Kind\Html as HtmlKind;

final class Emphasis implements HtmlKind
{
    public function __construct(
        /** @var HtmlKind[] */
        public readonly array $nodes,
    ) {
    }

    public function compile(): Html
    {
        $html = new Html('');

        foreach ($this->nodes as $node) {
            $html = $html->concatenateAfter($node->compile());
        }

        return new Html(\sprintf('<em>%s</em>', $html->value));
    }
}
