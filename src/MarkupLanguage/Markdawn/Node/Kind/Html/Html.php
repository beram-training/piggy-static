<?php

declare(strict_types=1);

namespace beram\PiggyStatic\MarkupLanguage\Markdawn\Node\Kind\Html;

use beram\PiggyStatic\MarkupLanguage\Markdawn\Compiled;
use beram\PiggyStatic\MarkupLanguage\Markdawn\Node\Kind\Html as HtmlKind;

final class Html implements HtmlKind
{
    public function __construct(
        public readonly string $value,
    ) {
    }

    public function compile(): Compiled\Html
    {
        return new Compiled\Html($this->value);
    }
}
