<?php

declare(strict_types=1);

namespace beram\PiggyStatic\MarkupLanguage\Markdawn\Node\Kind\Html;

use beram\PiggyStatic\MarkupLanguage\Markdawn\Compiled\Html;
use beram\PiggyStatic\MarkupLanguage\Markdawn\Node\Kind\Html as HtmlKind;

final class Image implements HtmlKind
{
    public function __construct(
        public readonly Image\Src $src,
        public readonly Image\Alt $alt,
    ) {
    }

    public function compile(): Html
    {
        return new Html(\sprintf(
            '<img%s%s>%s',
            (string) $this->src->compile(),
            (string) $this->alt->compile(),
            \PHP_EOL,
        ));
    }
}
