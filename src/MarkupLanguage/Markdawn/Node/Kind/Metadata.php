<?php

declare(strict_types=1);

namespace beram\PiggyStatic\MarkupLanguage\Markdawn\Node\Kind;

use beram\PiggyStatic\MarkupLanguage\Markdawn\Node;

interface Metadata extends Node
{
}
