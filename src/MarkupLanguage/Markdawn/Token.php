<?php

declare(strict_types=1);

namespace beram\PiggyStatic\MarkupLanguage\Markdawn;

final class Token
{
    public function __construct(
        public readonly TokenType $type,
        public readonly string $value,
    ) {
    }
}
