<?php

declare(strict_types=1);

namespace beram\PiggyStatic\MarkupLanguage\Markdawn;

enum TokenType
{
    case LINE_BLANK;
    case SPACE;
    case TEXT;
    case EOF;
    case EOL;
    case HEADING1;
    case HEADING2;
    case HEADING3;
    case HEADING4;
    case HEADING5;
    case HEADING6;
    case STRONG;
    case EMPHASIS;
    case STRIKETHROUGH;
    case LINK;
    case IMAGE;
    case HORIZONTAL_RULE;
    case CODE_BEGIN;
    case CODE_END;
    case UNORDERED_LIST_BULLET;
    case BLOCKQUOTE_BEGIN;
    case BLOCKQUOTE_END;
    case COMMENT_BEGIN;
    case COMMENT_END;
    case HTML_BEGIN;
    case HTML_END;
    case METADATA_BEGIN;
    case METADATA_PROPERTY;
    case METADATA_OPERATOR_ASSIGNMENT;
    case METADATA_VALUE;
    case METADATA_END;
}
