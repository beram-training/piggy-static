<?php

declare(strict_types=1);

namespace beram\PiggyStatic\MarkupLanguage\Markdawn;

use beram\PiggyStatic\MarkupLanguage\Markdawn\Exception\TokenStreamException;

final class TokenStream
{
    private int $current = 0;

    public function __construct(
        /** @var Token[] */
        public readonly array $tokens,
    ) {
    }

    public function next(): Token
    {
        return $this->tokens[++$this->current] ?? throw new TokenStreamException('Unexpected end of file.');
    }

    public function tryNext(): ?Token
    {
        return $this->tokens[++$this->current] ?? null;
    }

    /**
     * Sets the pointer to the next token with the desired type.
     *
     * @return Token[]
     *                 The tokens until the desired type has been found.
     */
    public function nextUntil(TokenType $type): array
    {
        $tokens = [];
        while ($type !== $this->getCurrent()->type) {
            $tokens[] = $this->getCurrent();
            $this->next();
        }

        return $tokens;
    }

    /**
     * @return Token[]
     */
    public function tryNextUntil(TokenType $type): array
    {
        $tokens = [];
        while ($this->isAbleToContinueLoop() && $type !== $this->getCurrent()->type) {
            $tokens[] = $this->getCurrent();
            $this->tryNext();
        }

        return $tokens;
    }

    public function lookAt(int $nth): Token
    {
        return $this->tokens[$this->current + $nth] ?? throw new TokenStreamException(\sprintf('Cannot look at %d-nth token', $nth));
    }

    public function getCurrent(): Token
    {
        return $this->tokens[$this->current] ?? throw new TokenStreamException('Current token not found.');
    }

    public function isEOF(): bool
    {
        return TokenType::EOF === $this->getCurrent()->type;
    }

    public function hasNext(): bool
    {
        return isset($this->tokens[$this->current + 1]);
    }

    public function isLast(): bool
    {
        return isset($this->tokens[$this->current]) && !isset($this->tokens[$this->current + 1]);
    }

    public function isAbleToContinueLoop(): bool
    {
        return $this->hasNext() || $this->isLast();
    }
}
