<?php

declare(strict_types=1);

namespace beram\PiggyStatic\MarkupLanguage\Markdawn;

use beram\PiggyStatic\Internal\Regex;
use beram\PiggyStatic\MarkupLanguage\Markdawn\Exception\CannotExtractLineFromSourceException;

final class Lexer
{
    /**
     * @return Token[]
     */
    public function tokenize(Source $source): array
    {
        $tokens = [];
        while (!$source->file->eof()) {
            $line = $this->extractCurrentLine($source);

            if ('' === $line) {
                $tokens[] = new Token(TokenType::LINE_BLANK, '');
                $source->file->next();
                continue;
            }

            if (\str_starts_with($line, '@metadata')) {
                $tokens = \array_merge($tokens, $this->tokenizeMetadata($source));
                $source->file->next();
                continue;
            }

            if (\str_starts_with($line, '# ')) {
                $tokens = \array_merge($tokens, $this->tokenizeHeadingX($line, TokenType::HEADING1, '#'));
                $source->file->next();
                continue;
            }

            if (\str_starts_with($line, '## ')) {
                $tokens = \array_merge($tokens, $this->tokenizeHeadingX($line, TokenType::HEADING2, '##'));
                $source->file->next();
                continue;
            }

            if (\str_starts_with($line, '### ')) {
                $tokens = \array_merge($tokens, $this->tokenizeHeadingX($line, TokenType::HEADING3, '###'));
                $source->file->next();
                continue;
            }

            if (\str_starts_with($line, '#### ')) {
                $tokens = \array_merge($tokens, $this->tokenizeHeadingX($line, TokenType::HEADING4, '####'));
                $source->file->next();
                continue;
            }

            if (\str_starts_with($line, '##### ')) {
                $tokens = \array_merge($tokens, $this->tokenizeHeadingX($line, TokenType::HEADING5, '#####'));
                $source->file->next();
                continue;
            }

            if (\str_starts_with($line, '###### ')) {
                $tokens = \array_merge($tokens, $this->tokenizeHeadingX($line, TokenType::HEADING6, '######'));
                $source->file->next();
                continue;
            }

            if ('---' === $line) {
                $tokens[] = new Token(TokenType::HORIZONTAL_RULE, $line);
                $tokens[] = new Token(TokenType::EOL, \PHP_EOL);
                $source->file->next();
                continue;
            }

            if (\str_starts_with($line, '```')) {
                $tokens = \array_merge($tokens, $this->tokenizeCode($line, $source));
                $source->file->next();
                continue;
            }

            if (\str_starts_with($line, '>')) {
                $tokens = \array_merge($tokens, $this->tokenizeBlockquote($source));
                $source->file->next();
                continue;
            }

            if (\str_starts_with($line, '@html')) {
                $tokens = \array_merge($tokens, $this->tokenizeHtml($source));
                $source->file->next();
                continue;
            }

            $tokens = \array_merge($tokens, $this->tokenizeEmphasis($line));

            $tokens[] = new Token(TokenType::EOL, \PHP_EOL);
            $source->file->next();
        }

        $tokens[] = new Token(TokenType::EOF, '');

        return $tokens;
    }

    private function extractCurrentLine(Source $source): string
    {
        return \trim($this->extractCurrentRawLine($source));
    }

    private function extractCurrentRawLine(Source $source): string
    {
        $line = $source->file->current();
        if (!\is_string($line)) {
            throw new CannotExtractLineFromSourceException('Something went wrong extracting the line from the source.');
        }

        return $line;
    }

    /**
     * @return \Generator<string>
     */
    private function extractLineUntilMatch(Source $source, string $match): \Generator
    {
        while (!Regex::match($match, $line = $this->extractCurrentLine($source)) || $source->file->eof()) {
            yield $line;
            $source->file->next();
        }
        yield $this->extractCurrentLine($source);
    }

    /**
     * @return \Generator<string>
     */
    private function extractRawLineUntilMatch(Source $source, string $match): \Generator
    {
        while (!Regex::match($match, $line = $this->extractCurrentRawLine($source)) || $source->file->eof()) {
            yield $line;
            $source->file->next();
        }
        yield $this->extractCurrentRawLine($source);
    }

    /**
     * @return \Generator<string>
     */
    private function extractLineUntilDoesNotMatch(Source $source, string $match): \Generator
    {
        while (Regex::match($match, $line = $this->extractCurrentLine($source))) {
            yield $line;
            $source->file->next();
        }
    }

    /**
     * @return Token[]
     */
    private function tokenizeHeadingX(string $line, TokenType $type, string $level): array
    {
        return [
            new Token($type, $level),
            new Token(TokenType::SPACE, ' '),
            new Token(TokenType::TEXT, \ltrim($line, $level.' ')),
            new Token(TokenType::EOL, \PHP_EOL),
        ];
    }

    /**
     * @return Token[]
     */
    private function tokenizeCode(string $line, Source $source): array
    {
        $tokens = [
            new Token(TokenType::CODE_BEGIN, $line),
        ];
        $source->file->next();
        $codeText = '';

        foreach ($this->extractRawLineUntilMatch($source, '#^```$#') as $lineUntilMatch) {
            if (Regex::match('#^```$#', $lineUntilMatch)) {
                $tokens[] = new Token(TokenType::TEXT, $codeText);
                $tokens[] = new Token(TokenType::CODE_END, \trim($lineUntilMatch));
                continue;
            }
            $codeText .= \rtrim($lineUntilMatch).\PHP_EOL;
        }

        $tokens[] = new Token(TokenType::EOL, \PHP_EOL);

        return $tokens;
    }

    /**
     * @return Token[]
     */
    private function tokenizeBlockquote(Source $source): array
    {
        $tokens = [
            new Token(TokenType::BLOCKQUOTE_BEGIN, '> '),
        ];

        foreach ($this->extractLineUntilDoesNotMatch($source, '#^>.*#') as $lineUntilMatch) {
            if ('>' === $lineUntilMatch) {
                $tokens[] = new Token(TokenType::LINE_BLANK, '');
                continue;
            }

            // @todo support other things than text
            // @see Parser::parseBlockquote()
            $tokens[] = new Token(TokenType::TEXT, \ltrim($lineUntilMatch, '> '));
            $tokens[] = new Token(TokenType::EOL, \PHP_EOL);
        }

        $tokens[] = new Token(TokenType::BLOCKQUOTE_END, '> ');

        return $tokens;
    }

    /**
     * @return Token[]
     */
    private function tokenizeHtml(Source $source): array
    {
        $tokens = [
            new Token(TokenType::HTML_BEGIN, '@html'),
        ];
        $source->file->next();
        $text = '';

        foreach ($this->extractLineUntilMatch($source, '#^\@endhtml$#') as $lineUntilMatch) {
            if ('@endhtml' === $lineUntilMatch) {
                $tokens[] = new Token(TokenType::TEXT, $text);
                $tokens[] = new Token(TokenType::HTML_END, $lineUntilMatch);
                continue;
            }
            $text .= $lineUntilMatch.\PHP_EOL;
        }

        $tokens[] = new Token(TokenType::EOL, \PHP_EOL);

        return $tokens;
    }

    /**
     * @return Token[]
     */
    private function tokenizeMetadata(Source $source): array
    {
        $tokens = [
            new Token(TokenType::METADATA_BEGIN, '@metadata'),
        ];
        $source->file->next();

        foreach ($this->extractLineUntilMatch($source, '#^\@endmetadata$#') as $lineUntilMatch) {
            if ('@endmetadata' === $lineUntilMatch) {
                $tokens[] = new Token(TokenType::METADATA_END, $lineUntilMatch);
                continue;
            }

            [$property, $value] = \explode(':', $lineUntilMatch);
            $tokens[] = new Token(TokenType::METADATA_PROPERTY, \trim($property));
            $tokens[] = new Token(TokenType::METADATA_OPERATOR_ASSIGNMENT, ':');
            $tokens[] = new Token(TokenType::METADATA_VALUE, \trim($value));
            $tokens[] = new Token(TokenType::EOL, \PHP_EOL);
        }

        $tokens[] = new Token(TokenType::EOL, \PHP_EOL);

        return $tokens;
    }

    /**
     * Emphasis here corresponds to: strong, italic (aka emphasis in html) and strikethrough.
     *
     * @return Token[]
     */
    private function tokenizeEmphasis(string $line): array
    {
        $tokens = [];

        $parts = \preg_split(
            '{
    ([^\*~\!\[\]\(\)]*)
        (\*\*|\*|~~)(.*?)(\*\*|\*|~~) # strong, emphasis and strikethrough
        |
        (\!\[[^(\!\[)]*\]\([^(\!\[)]*\)) # images
        |
        (\[[^\[\]\(\)]*\]\([^\[\]\(\)]*\)) # links
    ([^\*~\!\[\]\(\)]*)
    }uxs',
            $line, 0, \PREG_SPLIT_DELIM_CAPTURE);
        foreach ($parts as $part) {
            if ('**' === $part) {
                $tokens[] = new Token(TokenType::STRONG, $part);
                continue;
            }

            if ('*' === $part) {
                $tokens[] = new Token(TokenType::EMPHASIS, $part);
                continue;
            }

            if ('~~' === $part) {
                $tokens[] = new Token(TokenType::STRIKETHROUGH, $part);
                continue;
            }

            if (Regex::match('#^\!\[[^(\!\[)]*\]\([^(\!\[)]*\)$#', $part)) {
                $tokens[] = new Token(TokenType::IMAGE, $part);
                continue;
            }

            if (Regex::match('#^\[[^\[\]\(\)]*\]\([^\[\]\(\)]*\)$#', $part)) {
                $tokens[] = new Token(TokenType::LINK, $part);
                continue;
            }

            if ('' === $part) {
                continue;
            }

            $tokens[] = new Token(TokenType::TEXT, $part);
        }

        return $tokens;
    }
}
