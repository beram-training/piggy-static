<?php

declare(strict_types=1);

namespace beram\PiggyStatic\MarkupLanguage\Markdawn\Compiled\Metadata;

final class Property
{
    public function __construct(
        public readonly string $name,
        public readonly string $value,
    ) {
    }
}
