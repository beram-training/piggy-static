<?php

declare(strict_types=1);

namespace beram\PiggyStatic\MarkupLanguage\Markdawn\Compiled;

final class Document
{
    public function __construct(
        public readonly Html $html,
        public readonly Metadata $metadata,
    ) {
    }
}
