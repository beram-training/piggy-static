<?php

declare(strict_types=1);

namespace beram\PiggyStatic\MarkupLanguage\Markdawn\Compiled;

use beram\PiggyStatic\MarkupLanguage\Markdawn\Compiled\Metadata\Property;
use beram\PiggyStatic\MarkupLanguage\Markdawn\Exception\UndefinedPropertyException;

final class Metadata
{
    public function __construct(
        /** @var Property[] */
        private readonly array $properties,
    ) {
    }

    public function hasProperty(string $name): bool
    {
        return isset($this->properties[$name]);
    }

    public function getProperty(string $name): Property
    {
        return $this->properties[$name] ?? throw new UndefinedPropertyException($name);
    }
}
