<?php

declare(strict_types=1);

namespace beram\PiggyStatic\MarkupLanguage\Markdawn\Compiled;

final class Html
{
    public function __construct(
        public readonly string $value
    ) {
    }

    public function concatenateAfter(self $html): self
    {
        return new self($this->value.$html->value);
    }

    public function __toString(): string
    {
        return $this->value;
    }
}
