<?php

declare(strict_types=1);

namespace beram\PiggyStatic\MarkupLanguage\Markdawn;

use beram\PiggyStatic\MarkupLanguage\Markdawn\Node\Document;

final class Compiler
{
    public function compile(Document $document): Compiled\Document
    {
        return $document->compile();
    }
}
