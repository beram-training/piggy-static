<?php

declare(strict_types=1);

namespace beram\PiggyStatic\MarkupLanguage\Markdawn;

final class Source
{
    public function __construct(
        public readonly \SplFileObject $file,
        public readonly string $path,
    ) {
    }

    public static function fromFilePath(string $filepath): self
    {
        return new self(
            new \SplFileObject($filepath, 'r'),
            \realpath($filepath),
        );
    }
}
