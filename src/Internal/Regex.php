<?php

declare(strict_types=1);

namespace beram\PiggyStatic\Internal;

final class Regex
{
    public static function unquote(string $string): string
    {
        $specialCharacters = ['.', '\\', '+', '*', '?', '[', ']', '^', '$', '(', ')', '{', '}', '=', '!', '<', '>', '|', ':', '-', '#'];

        return \strtr($string, \array_combine(\array_map(static fn (string $char): string => \sprintf('\\%s', $char), $specialCharacters), $specialCharacters));
    }

    public static function match(string $pattern, string $subject): bool
    {
        $matches = [];
        \preg_match($pattern, $subject, $matches);

        return [] !== $matches;
    }
}
