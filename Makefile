.DEFAULT_GOAL := help

# Colors
COLOR_END ?= \033[0m
COLOR_RED ?= \033[31m
COLOR_GREEN ?= \033[32m
COLOR_YELLOW ?= \033[33m
COLOR_CYAN ?= \033[36m
# Inverted, i.e. colored backgrounds
COLOR_IRED ?= \033[0;30;41m
COLOR_IGREEN ?= \033[0;30;42m
COLOR_IYELLOW ?= \033[0;30;43m
COLOR_ICYAN ?= \033[0;30;46m

################################################################################
##@ Miscellaneous
################################################################################

.PHONY: help
help: ## Show this help.
	@awk 'BEGIN {FS = ":.*##"; printf "Usage: make ${COLOR_GREEN}[target]${COLOR_END}\n\nAvailable ${COLOR_GREEN}targets${COLOR_END} by ${COLOR_CYAN}category${COLOR_END}:\n"} \
		/^[a-zA-Z_-]+:.*?##/ { \
			printf "\t${COLOR_GREEN}%-10s${COLOR_END}\t%s\n", $$1, $$2 \
		} \
		/^##@/ { \
			printf "\n${COLOR_CYAN}%s${COLOR_END}\n", substr($$0, 5) \
		}' \
		$(MAKEFILE_LIST)

################################################################################
##@ Project
################################################################################

.PHONY: install
install: ## to install the project
	./tools/phive install
	./tools/composer install

.PHONY: clean
clean: ## Clean your repository (delete files ignored by git)
	git ls-files --others --ignored --exclude-from=.gitignore --exclude-per-directory=.gitignore --directory | awk -v quote='"' -v OFS="" '!/.idea/ {print quote,$$0,quote}' | xargs rm -rf

################################################################################
##@ Quality
################################################################################

.PHONY: quality-check-min
quality-check-min: ## Let's check if it pass the minimal quality gate :P
	PHP_CS_FIXER_IGNORE_ENV=1 php tools/php-cs-fixer fix --dry-run --diff
	php tools/psalm

.PHONY: quality-check
quality-check: ## Let's check if it pass the quality gate :P
	${MAKE} quality-check-min
	php tools/phpunit.phar

.PHONY: quality-fix
quality-fix: ## Automatically fix what could be
	PHP_CS_FIXER_IGNORE_ENV=1 php tools/php-cs-fixer fix
