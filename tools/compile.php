<?php

declare(strict_types=1);

/**
 * @file
 * Compile to phar file.
 *
 * Must be run from the root.
 *
 * Usage: php --define phar.readonly=0 tools/compile.php
 */

return (new class() {
    public function __invoke(string $file): int
    {
        $this->clean($file);
        @\mkdir(\dirname($file), 0755, true);

        $phar = new \Phar($file);
        $phar->buildFromDirectory(__DIR__.'/../', $this->excludeFromPharPattern());
        $phar->setAlias('piggy-static-generate.phar');
        $phar->setStub(<<<'CODE'
<?php

Phar::mapPhar();

include 'phar://piggy-static-generate.phar/bin/generate.php';

__HALT_COMPILER(); ?>
CODE);
        $phar->compressFiles(\Phar::GZ);

        return 0;
    }

    private function clean(string $file): void
    {
        if (\file_exists($file)) {
            \unlink($file);
        }

        $gz = \sprintf('%.gz', $file);
        if (\file_exists($gz)) {
            \unlink($gz);
        }
    }

    private function excludeFromPharPattern(): string
    {
        $fromRoot = fn (string $file): string => \preg_quote(__DIR__.'/../'.$file, '#');

        return \sprintf('#(%s)#', \implode(
            '|',
            [
                '^'.$fromRoot('src'),
                '^'.$fromRoot('config'),
                '^'.$fromRoot('bin'),
                '^'.$fromRoot('autoload.php').'$',
            ],
        ));
    }
})('build/piggy-static-generate.phar');
