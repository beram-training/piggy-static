#!/usr/bin/env php
<?php

declare(strict_types=1);

require_once __DIR__.'/../autoload.php';

return ((new \beram\PiggyStatic\Kernel())->container->get(\beram\PiggyStatic\WebsiteGenerator\Command\Generate::class))();
